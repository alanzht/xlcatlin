[CmdletBinding()]
param(
    [parameter(Mandatory = $true)]
    [string]$SubscriptionAlias,

    [switch]$DeployResourceGroups,
    [switch]$DeployvNetPeering,
    [switch]$DeployRunbooks,
    [switch]$DeployTags,
    [switch]$DeployPolicies,
    [switch]$DeployArtifacts
)

. .\XLCFunctions.ps1

$SubDetails = Get-XLCAzureDetails -SubscriptionAlias $SubscriptionAlias

Write-Host ""
Write-Host "Deploy: $($SubDetails.Subscription.Name)..."
Write-Host ""
Write-Host "Subscription Details:"
$SubDetails 
Write-Host ""

Write-Host "Create Parameter File..."
New-XLCAzureParameterFile `
    -SubscriptionName   $SubDetails.Subscription.Name `
    -SubscriptionType   $SubDetails.Subscription.Type `
    -Location           $SubDetails.Region `
    -verbose
Write-Host ""

if ($DeployResourceGroups)
{
    Write-Host "Create Resource Group and Resources..."
    Set-XLCAzureDeployResourceGroups `
        -AppSubscriptionName    $SubDetails.Subscription.Name `
        -location               $SubDetails.Region `
        -ParameterFile          ".\$($SubDetails.Subscription.Name)-parameters.json" `
        -TemplateFolder         ".\Environments\$($SubDetails.Subscription.Type)\resource-groups" `
        -ResourceGroupList      $SubDetails.Subscription.ResourceGroupList `
        -verbose
    Write-Host ""
}

if ($DeployvNetPeering)
{
    Set-XLCAzurevNetPeering `
        -vNETPeeringName        $SubDetails.Subscription.vNetPeeringName `
        -FromSubscriptionName   $SubDetails.PCSSubscription.Name `
        -FromResourceGroupName  $SubDetails.PCSSubscription.ResourceGroups.MainNet `
        -ToSubscriptionName     $SubDetails.Subscription.Name `
        -ToResourceGroupName    $SubDetails.Subscription.ResourceGroups.MainNet `
        -verbose
    Write-Host ""
}

if ($DeployRunbooks)
{
    Write-Host "Create Run Books..."
    Set-XLCAzurePCSRunbooks `
        -PCSSubscriptionName    $SubDetails.PCSSubscription.Name `
        -ResourceGroupName      $SubDetails.PCSSubscription.ResourceGroups.Operations `
        -parameters             ".\$($SubDetails.Subscription.Name)-parameters.json" `
        -TemplateFolder         ".\Environments\$($SubDetails.Subscription.Type)\resource-groups" `
        -verbose
    Write-Host ""
}

if ($DeployTags)
{
    Write-Host "Create Tags..."
    Set-XLCAzureTags `
        -AppFolder           ".\Environments\$($SubDetails.Subscription.Type)\parameters\$($SubDetails.Subscription.Name)" `
        -SubscriptionName    $SubDetails.Subscription.Name `
        -verbose
    Write-Host ""
}

if ($DeployPolicies)
{
    Write-Host "Create Policies..."
    Set-XLCAzurePolicies `
        -SubscriptionName $SubDetails.Subscription.Name
    Write-Host ""
}

if ($DeployArtifacts)
{   
    Write-Host "Deploy Artifacts..."
    Set-XLCAzurePCSUploadArtifacts `
        -location                       $SubDetails.Region `
        -PCSSubscriptionName            $SubDetails.artifactrepository.subscriptionname `
        -PCSDeployAidRGName             $SubDetails.artifactrepository.resourcegroupname `
        -PCSDeployAidStorageAccountName $SubDetails.artifactrepository.storageaccount `
        -UploadListJsonFile             $SubDetails.artifactrepository.UploadListJsonFile `
        -Verbose  
        Write-Host ""    
}

<#
  funcname `
        -vm                 $VM `
        -resourceGroup      $RG `
        -aadClientID        $SubDetails.chef.servieprincipal `
        -aadClientSecret    $SubDetails.chef.servieprincipalsecret `
        -keyVaultName       $SubDetails.chef.keyVaultName `
        -validatorName      $SubDetails.chef.validatorName `
        -chefServer         $SubDetails.chef.chefServer `
        -secretName         $SubDetails.chef.secretName `
        -runList            $SubDetails.chef.runListWindows `
        -chefClientUri      $SubDetails.chef.chefClientUri `
        -ChefEnvironment    $SubDetails.chef.chefenvironment `
        -verbose

#>