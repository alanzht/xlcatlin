domain_join 'xlc-domain-join' do
    domain          node['activedirectory']['domain']
    domain_user     node['activedirectory']['user']
    domain_password node['activedirectory']['password']
    ou "OU=%{container},%{domain}" % { container: (node['platform']=="redhat" ? node['activedirectory']['ou']['container']['rhel'] : node['activedirectory']['ou']['container']['windows']), domain: node['activedirectory']['ou']['domain'] }
    update_hostname false
    visual_warning true
    hide_sensitive true
    double_reboot true
    action :join
    not_if { (node['domain']==node['activedirectory']['domain']) }
  end