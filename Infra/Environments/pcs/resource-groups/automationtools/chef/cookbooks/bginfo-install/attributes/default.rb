default['artifactory']['prefix'] = ""
default['artifactory']['sasToken'] = ""
default['package']['bginfo']['url'] = "%{artifactoryPrefix}/artifacts/windows/2016/%{packageFile}?%{artifactorySASToken}"
default['package']['bginfo']['file'] = "Microsoft-BGInfo-4.25_100.zip"
default['package']['bginfo']['winfriendlyname'] = 'bginfo'
default['package']['bginfo']['install'] = 'Install.cmd'