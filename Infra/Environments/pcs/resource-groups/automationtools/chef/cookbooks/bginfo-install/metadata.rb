name 'bginfo-install'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license ''
description 'Installs/Configures bginfo-install'
long_description 'Installs/Configures bginfo-install'
version '0.1.1'
chef_version '>= 12.14' if respond_to?(:chef_version)
depends 'windows'
# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/pcs-infra-chef/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/pcs-infra-chef'

# "dependencies": {
#     "windows": ">= 1.30.0"
# },