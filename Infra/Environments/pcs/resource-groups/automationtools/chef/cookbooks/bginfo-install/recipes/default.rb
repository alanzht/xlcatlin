#
# Cookbook:: XLC_Windows2016
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

::Chef::Recipe.send(:include, Windows::Helper)

# Check it is installed
is_bginfo_installed = registry_data_exists?('HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run', 
    { :name => 'bginfo',  :type => :string, :data => '"C:\Program Files\BGInfo\bginfo64.exe" "C:\Program Files\BGInfo\bginfo.bgi" /silent /accepteula /timer:0' } )

# Unzip
windows_zipfile File.join(Chef::Config[:file_cache_path], node['package']['bginfo']['winfriendlyname']) do
    source node['package']['bginfo']['url'] % { artifactoryPrefix: node['artifactory']['prefix'], packageFile: node['package']['bginfo']['file'], artifactorySASToken: node['artifactory']['sasToken'] }
    action :unzip
    not_if { is_bginfo_installed }
end

# Install
workingdirectory = win_friendly_path(File.join(Chef::Config[:file_cache_path], node['package']['bginfo']['winfriendlyname'], node['package']['bginfo']['install']))
batch 'run-bginfo-installer' do
    code workingdirectory
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['bginfo']['winfriendlyname'])) }
end

directory File.join(Chef::Config[:file_cache_path], node['package']['bginfo']['winfriendlyname']) do
    recursive true
    action :delete
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['bginfo']['winfriendlyname'])) }
end
