default['artifactory']['prefix'] = ""
default['artifactory']['sasToken'] = ""
default['package']['dot-net']['url'] = "%{artifactoryPrefix}/artifacts/windows/2016/%{packageFile}?%{artifactorySASToken}"
default['package']['dot-net']['file'] = "Microsoft.Net-4.7.2_1.0.0.zip"
default['package']['dot-net']['winfriendlyname'] = 'dotnet472'
default['package']['dot-net']['install'] = 'install.cmd'