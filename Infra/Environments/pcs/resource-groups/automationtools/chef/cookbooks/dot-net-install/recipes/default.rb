#
# Cookbook:: dot-net-install
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

##::Chef::Recipe.send(:include, Chef::Mixin::PowershellOut)

#script = <<-EOH
#    Get-ChildItem "HKLM:SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\" | Get-ItemPropertyValue -Name Release | ForEach-Object {$_ -ge 461808}
#EOH

# Check if old version is installed - old version is 394802 while desired is 461814

# Unzip
windows_zipfile File.join(Chef::Config[:file_cache_path], node['package']['dot-net']['winfriendlyname']) do
    source node['package']['dot-net']['url'] % { artifactoryPrefix: node['artifactory']['prefix'], packageFile: node['package']['dot-net']['file'], artifactorySASToken: node['artifactory']['sasToken'] }
    action :unzip
    only_if { registry_data_exists?('HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full',{ :name => 'Release', :type => :dword, :data => 394802 } ) }
end

# Install
workingdirectory = win_friendly_path(File.join(Chef::Config[:file_cache_path], node['package']['dot-net']['winfriendlyname'], node['package']['dot-net']['install']))
batch 'run-dotnet-installer' do
    code workingdirectory
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['dot-net']['winfriendlyname'])) }
end

# Reboot node
reboot 'app_requires_reboot' do
    action :request_reboot
    reason '.NET v4 Installed - Reboot Required'
    delay_mins 5
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['dot-net']['winfriendlyname'])) }
end

# Cleanup
directory File.join(Chef::Config[:file_cache_path], node['package']['dot-net']['winfriendlyname']) do
    recursive true
    action :delete
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['dot-net']['winfriendlyname'])) }
end
