# mcafee_install
Installs McAfee onto the node. Supports either RHEL or Windows.

#### Version History

#####Version 0.1.2
Changed McAfee payload to the most recent version so that the VSE communicated with the newest EPO for both RHEL and Windows.
