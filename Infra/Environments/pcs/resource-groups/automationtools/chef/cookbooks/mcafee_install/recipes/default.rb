#
# Cookbook:: mcafee_install
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

case node['platform']
when 'windows'
    include_recipe 'mcafee_install::windows'
when 'redhat'
    include_recipe 'mcafee_install::redhat'
end