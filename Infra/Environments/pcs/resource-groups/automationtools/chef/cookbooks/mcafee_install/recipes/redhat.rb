#
# Cookbook:: mcafee_install
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

mcafee_exists = File.exist? '/etc/init.d/ma'

#create dir
directory '/tmp/mcafee' do
    owner 'root'
    group 'root'
    mode '0755'
    action :create
    not_if { mcafee_exists }
end
  
  # download
remote_file '/tmp/mcafee/mcafee.tar' do
    source node['package']['mcafee']['rhel']['url'] % { artifactoryPrefix: node['artifactory']['prefix'], packageFile: node['package']['mcafee']['rhel']['file'], artifactorySASToken: node['artifactory']['sasToken'] }
    action :create
    not_if { mcafee_exists }
end
  
  #extract
bash 'extract_mcafee' do
    cwd '/tmp/mcafee'
    code 'tar -xvf mcafee.tar'
    not_if { mcafee_exists }
end

#execute script
bash 'install_mcafee' do
    cwd '/tmp/mcafee'
    code <<-EOH
        chmod +x install.sh
        ./install.sh -i
    EOH
    not_if { mcafee_exists }
end
  