#
# Cookbook:: mcafee_install
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

::Chef::Recipe.send(:include, Windows::Helper)

is_mcafee_vs_installed = is_package_installed?(node['package']['mcafee']['windows']['name'])

# Unzip
windows_zipfile File.join(Chef::Config[:file_cache_path], node['package']['mcafee']['windows']['winfriendlyname']) do
    source node['package']['mcafee']['windows']['url'] % { artifactoryPrefix: node['artifactory']['prefix'], packageFile: node['package']['mcafee']['windows']['file'], artifactorySASToken: node['artifactory']['sasToken'] }
    action :unzip
    not_if { is_mcafee_vs_installed }
end

# Install
workingdirectory = win_friendly_path(File.join(Chef::Config[:file_cache_path], node['package']['mcafee']['windows']['winfriendlyname'], node['package']['mcafee']['windows']['install']))
batch 'run-mcafee-installer' do
    code workingdirectory
    not_if { is_mcafee_vs_installed }
end

# Cleanup
directory File.join(Chef::Config[:file_cache_path], node['package']['mcafee']['windows']['winfriendlyname']) do
    recursive true
    action :delete
end