default['artifactory']['prefix'] = ""
default['artifactory']['sasToken'] = ""
default['package']['qualysguard']['url'] = "%{artifactoryPrefix}/artifacts/windows/2016/%{packageFile}?%{artifactorySASToken}"
default['package']['qualysguard']['file'] = "QualysGuard%20Scan%20Cfg%201.0_1.00.zip"
default['package']['qualysguard']['winfriendlyname'] = 'qualysguard'
default['package']['qualysguard']['install'] = 'install.cmd'