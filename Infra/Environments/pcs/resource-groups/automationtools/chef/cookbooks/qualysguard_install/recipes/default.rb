#
# Cookbook:: XLC-Server-2016-Core-Build-v1.0
# Recipe:: QualysGuard_Scan_Cfg
#
# Copyright:: 2018, The Authors, All Rights Reserved.

# Check QualysGuard is installed
regkey = 'HKEY_LOCAL_MACHINE\\SOFTWARE\\qualysguard'
is_qualysguard_installed = registry_key_exists?(regkey, :x86_64 )
# Unzip
windows_zipfile File.join(Chef::Config[:file_cache_path], node['package']['qualysguard']['winfriendlyname']) do
    source node['package']['qualysguard']['url'] % { artifactoryPrefix: node['artifactory']['prefix'], packageFile: node['package']['qualysguard']['file'], artifactorySASToken: node['artifactory']['sasToken'] }
    action :unzip
    not_if { is_qualysguard_installed }
end

# Install
workingdirectory = win_friendly_path(File.join(Chef::Config[:file_cache_path], node['package']['qualysguard']['winfriendlyname'], node['package']['qualysguard']['install']))
batch 'run-qualysguard-installer' do
    code workingdirectory
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['qualysguard']['winfriendlyname'])) }
end

# Cleanup
directory File.join(Chef::Config[:file_cache_path], node['package']['qualysguard']['winfriendlyname']) do
    recursive true
    action :delete
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['qualysguard']['winfriendlyname'])) }
end

# Create key for installation detection
registry_key regkey do
    values [{
      name: 'qualysguardinstalled',
      type: :string,
      data: 'true'
    }]
    not_if { is_qualysguard_installed }
    recursive true
    action :create
  end
