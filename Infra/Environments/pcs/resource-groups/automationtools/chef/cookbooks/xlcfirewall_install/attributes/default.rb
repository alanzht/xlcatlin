default['artifactory']['prefix'] = ""
default['artifactory']['sasToken'] = ""
default['package']['xlcfirewall']['url'] = "%{artifactoryPrefix}/artifacts/windows/2016/%{packageFile}?%{artifactorySASToken}"
default['package']['xlcfirewall']['file'] = "XLC%20Firewall%20Cfg%201.0_1.00.zip"
default['package']['xlcfirewall']['winfriendlyname'] = 'xlcfirewall'
default['package']['xlcfirewall']['install'] = 'install.cmd'