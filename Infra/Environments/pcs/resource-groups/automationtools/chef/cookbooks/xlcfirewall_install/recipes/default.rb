#
# Cookbook:: xlcfirewall_install
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

# PowerShell Script to check if the Firewall Rule called XLC Default Rule already exists or not
psScript = '(Get-NetFirewallRule -DisplayName "XLC Default Rule*" | measure).Count -eq 0'

# Unzip
windows_zipfile File.join(Chef::Config[:file_cache_path], node['package']['xlcfirewall']['winfriendlyname']) do
    source node['package']['xlcfirewall']['url'] % { artifactoryPrefix: node['artifactory']['prefix'], packageFile: node['package']['xlcfirewall']['file'], artifactorySASToken: node['artifactory']['sasToken'] }
    action :unzip
    guard_interpreter :powershell_script
    only_if psScript
end

# Install
workingdirectory = win_friendly_path(File.join(Chef::Config[:file_cache_path], node['package']['xlcfirewall']['winfriendlyname'], node['package']['xlcfirewall']['install']))
batch 'run-xlcfirewall-installer' do
    code workingdirectory
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['xlcfirewall']['winfriendlyname'])) }
end

#Cleanup
directory File.join(Chef::Config[:file_cache_path], node['package']['xlcfirewall']['winfriendlyname']) do
    recursive true
    action :delete
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['xlcfirewall']['winfriendlyname'])) }
end
