default['artifactory']['prefix'] = ""
default['artifactory']['sasToken'] = ""
default['package']['xlctools']['url'] = "%{artifactoryPrefix}/artifacts/windows/2016/%{packageFile}?%{artifactorySASToken}"
default['package']['xlctools']['file'] = "XLC%20Tools%201.0_1.00.zip"
default['package']['xlctools']['winfriendlyname'] = 'xlctools'
default['package']['xlctools']['install'] = 'install.cmd'
default['package']['xlctools']['installlocation'] = 'C:/Program Files/XLC/Tools'