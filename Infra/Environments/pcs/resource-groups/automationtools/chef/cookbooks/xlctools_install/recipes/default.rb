#
# Cookbook:: xlctools_install
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

# Check XlC Tools is installed
is_xlctools_installed = ::File.directory?(File.join(node['package']['xlctools']['installlocation']))

# Unzip
windows_zipfile File.join(Chef::Config[:file_cache_path], node['package']['xlctools']['winfriendlyname']) do
    source node['package']['xlctools']['url'] % { artifactoryPrefix: node['artifactory']['prefix'], packageFile: node['package']['xlctools']['file'], artifactorySASToken: node['artifactory']['sasToken'] }
    action :unzip
    not_if { ::File.directory?(File.join(node['package']['xlctools']['installlocation'])) }
end

# Install
workingdirectory = win_friendly_path(File.join(Chef::Config[:file_cache_path], node['package']['xlctools']['winfriendlyname'], node['package']['xlctools']['install']))
batch 'run-xlctools-installer' do
    code workingdirectory
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['xlctools']['winfriendlyname'])) }
end

#Cleanup
directory File.join(Chef::Config[:file_cache_path], node['package']['xlctools']['winfriendlyname']) do
    recursive true
    action :delete
    only_if { ::File.exists?(File.join(Chef::Config[:file_cache_path], node['package']['xlctools']['winfriendlyname'])) }
end
