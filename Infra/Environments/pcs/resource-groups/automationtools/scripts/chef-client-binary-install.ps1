$MSIArguments = @(
    "/i"
    "chef-client-14.2.0-1-x64.msi"
    'ADDLOCAL="ChefClientFeature,ChefServiceFeature"'
    "/quiet"
    "/qna"
    "/log"
    "C:\Windows\Temp\chef_install.txt"
)
Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow