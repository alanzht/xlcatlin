#!/bin/bash

# Call getopt to validate the provided input. 
options=$(getopt -o u:p:f:l:e:o: -l username:,password:,first-name:,last-name:,email:,organization: -- "$@")
eval set -- "$options"
while true; do
    case "$1" in
    -u|--username)
        username=$2
        shift 2
        ;;
    -p|--password)
        user_password=$2
        shift 2
        ;;
    -f|--first-name)
        user_first_name=$2
        shift 2
        ;;
    -l|--last-name)
        user_last_name=$2
        shift 2
        ;;
    -e|--email)
        user_email=$2
        shift 2
        ;;
    -o|--organization)
        organization=$2
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *)
        echo "Not implemented"
        exit 1
        ;;
    esac
done

declare user_rsa_path="/tmp/$username.pem"
declare organization_rsa_path="/tmp/$organization.pem"
declare hostname="$(hostname)"

# ---------- START This part Needed in any Linux server ---------- #

echo "Unzip & Install RPM packages needed for encryption process to run"
mkdir -p /tmp/rhel/
unzip -o rhel.zip -d /tmp/rhel/
for filename in /tmp/rhel/*.rpm; do
    rpm -Uvh --nodeps --nosignature --nodigest $filename
done

# ---------- END This part Needed in any Linux server ---------- #


# ---------- START Install CHEF server + Plugins ---------- #

echo "Unzip RPM packages needed for chef server to run"
mkdir chef-server
unzip chef-server.zip -d chef-server

echo "Copy artifacts to /tmp/"
cp chef-server/opscode-* /tmp/
cp chef-server/chef-* /tmp/

echo "Check if CHEF already installed"
if rpm -qa | grep chef-server-core  2>&1 > /dev/null; then
  echo "CHEF server installed, removing old installation" 
  rpm -e chef-server-core-*
fi

echo "Change Firewall"
sudo firewall-cmd --permanent --zone public --add-service http
sudo firewall-cmd --permanent --zone public --add-service https
sudo firewall-cmd --reload

echo "Install Chef server"
sudo rpm -Uvh /tmp/chef-server-core-*.rpm 
sudo chef-server-ctl reconfigure
sudo chef-server-ctl user-create $username $user_first_name $user_last_name $user_email $user_password --filename $user_rsa_path
sudo chef-server-ctl org-create $organization $organization --association_user $username --filename $organization_rsa_path

echo "Install Chef Manage package"
chef-server-ctl install chef-manage --path /tmp/
chef-server-ctl reconfigure
chef-manage-ctl reconfigure --accept-license

echo "Install Chef Push Jobs package"
chef-server-ctl install opscode-push-jobs-server --path /tmp/
chef-server-ctl reconfigure
opscode-push-jobs-server-ctl reconfigure

echo "Install Chef Reporting package"
chef-server-ctl install opscode-reporting --path /tmp/
chef-server-ctl reconfigure
opscode-reporting-ctl reconfigure --accept-license

echo "Change host name and restart service"
grep -q -F "127.0.0.1 $hostname" /etc/hosts || echo "127.0.0.1 $hostname" >> /etc/hosts
chef-server-ctl restart

# ---------- END Install CHEF server + Plugins ---------- #


# ---------- START Move CHEF cookbooks to bookshelf ---------- #

echo "Unzip initial chef cookbooks"
mkdir -p cookbooks
unzip -o cookbooks.zip -d cookbooks
cp -R cookbooks/* /var/opt/opscode/bookshelf/data/bookshelf/

# ---------- END Move CHEF cookbooks to bookshelf ---------- #


# ---------- START Output the validator and CHEF host information ---------- #

echo "Clear STDOUT and print json object"
cert=`cat $organization_rsa_path`
cert64=$(printf "%s" "$cert" | base64)
serverIP=$(echo $(hostname -I) | xargs)
jsonOutput="{ \"chefServer\": { \"organization\": \"$organization\", \"validatorName\": \"$organization-validator\" , \"validator\": \"$cert64\", \"server\": \"$serverIP\", \"host\": \"https://$serverIP/organizations/$organization\" } }"
>stdout
echo  $jsonOutput 1>stdout

# ---------- END Output the validator and CHEF host information ---------- #