param (
    [string]$sourceName,
    [string]$StorageAccountName,
    [string]$StorageKey
)

# Unzip XL-APPS
Expand-Archive xl-apps.zip -DestinationPath C:\windows\Temp -Force

# Install Chef client
Expand-Archive chef-client.zip -DestinationPath .\ -Force
$path = Resolve-Path ".\chef-client-*.msi" 
$MSIArguments = @(
    "/i"
    "$path"
    "/quiet"
    "/qna"
    "/log"
    "C:\Windows\Temp\chef_client_install.txt"
)
Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow

# Install push-jobs-client
$path =  Resolve-Path ".\push-jobs-*.msi"
$MSIArguments = @(
    "/i"
    "$path"
    "/quiet"
    "/qna"
    "/log"
    "C:\Windows\Temp\chef_push_jobs_install.txt"
)
Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow

# Install Az-Log
Expand-Archive az-log.zip -DestinationPath .\ -Force
$MSIArguments = @(
    "/i"
    "AzureLogIntegration.msi"
    "/quiet"
    "/qna"
    "/log"
    "C:\Windows\Temp\azlog_install.txt"
)
Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow

Start-Sleep -s 30

$logIntegrationFolder = "C:\Program Files\Microsoft Azure Log Integration"

cd $logIntegrationFolder

Import-module -Name .\AZLOGDll.dll

Set-AzlogAzureEnvironment -Name AzureCloud

if ( Get-AzLogEventSource | Select-String -Pattern $sourceName -InputObject {$_.name} ){
    Remove-AzLogEventSource $sourceName
}

Add-AzLogEventSource -Name $sourceName -StorageAccount $StorageAccountName -StorageKey $StorageKey

Get-AzLogEventSource