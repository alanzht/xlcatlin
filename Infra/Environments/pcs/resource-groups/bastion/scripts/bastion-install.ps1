# Unzip XL-APPS
Expand-Archive xl-apps.zip -DestinationPath C:\windows\Temp -Force

# Install Chef client
Expand-Archive chef-client.zip -DestinationPath .\ -Force
$path = Resolve-Path ".\chef-client-*.msi" 
$MSIArguments = @(
    "/i"
    "$path"
    "/quiet"
    "/qna"
    "/log"
    "C:\Windows\Temp\chef_client_install.txt"
)
Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow

# Install push-jobs-client
$path =  Resolve-Path ".\push-jobs-*.msi"
$MSIArguments = @(
    "/i"
    "$path"
    "/quiet"
    "/qna"
    "/log"
    "C:\Windows\Temp\chef_push_jobs_install.txt"
)
Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow