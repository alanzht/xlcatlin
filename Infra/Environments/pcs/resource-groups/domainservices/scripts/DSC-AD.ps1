# Cloud Technology Partners
# Description: PS script to create a domain
# File Name: DSC-AD.ps1
# Author: Liam Smith

configuration ADCreate
{             
   param             
    (              
        [Parameter(Mandatory)]             
        [pscredential]$safemodeAdministratorCred,      
        [Parameter(Mandatory)]             
        [string]$domainName,          
        [Parameter(Mandatory)]            
        [pscredential]$domainCred
    )             
            
    Import-DscResource -ModuleName PSDesiredStateConfiguration, xActiveDirectory, xStorage
            
    Node $AllNodes.Where{$_.Role -eq "DC"}.Nodename             
    {
        LocalConfigurationManager            
        {            
            ConfigurationMode = 'ApplyAndAutoCorrect'
            RebootNodeIfNeeded = $true
            ActionAfterReboot = 'ContinueConfiguration'
            AllowModuleOverwrite = $true          
        }            
            
        File ADFiles            
        {            
            DestinationPath = 'C:\NTDS'            
            Type = 'Directory'            
            Ensure = 'Present'            
        }            
                    
        WindowsFeature ADDSInstall             
        {             
            Ensure = "Present"             
            Name = "AD-Domain-Services"             
        }            
            
        # Optional GUI tools            
        WindowsFeature ADDSTools            
        {             
            Ensure = "Present"             
            Name = "RSAT-ADDS"             
        }            
            
        #Create the AD Domain
        # No slash at end of folder paths            
        # xADDomain CreateDomain             
        # {             
        #    DomainName = $domainName             
        #    DomainAdministratorCredential = $domainCred             
        #    SafemodeAdministratorPassword = $safemodeAdministratorCred            
        #    DatabasePath = 'C:\NTDS'            
        #    LogPath = 'C:\NTDS'            
        #    DependsOn = "[WindowsFeature]ADDSInstall","[File]ADFiles","[xDisk]NVolume"            
        #} 
                
        # Create an admin user so that the default Administrator account is not used
        #xADUser FirstUser
        #{
        #    DomainAdministratorCredential = $DomainAdmin1Creds
        #    DomainName = $domainName
        #    UserName = $AdminUser1Creds.UserName
        #    Password = $AdminUser1Creds
        #    Ensure = 'Present'
        #    DependsOn = '[xADDomain]CreateDomain'
        #}
        
        #xADGroup AddToDomainAdmins
        #{
        #    GroupName = 'Domain Admins'
        #    MembersToInclude = $domainCred.UserName
        #    Ensure = 'Present'
        #    DependsOn = '[xADDomain]CreateDomain'
        #}      
    }             
}                              