# Cloud Technology Partners
# Description: PS config file for assisting with creating
#              a domain
# File Name: Data-Disk.ps1
# Author: Liam Smith

# Configuration Data for AD  
@{
    AllNodes = @(
        @{
            NodeName="*"
            RetryCount = 20
            RetryIntervalSec = 30
            PSDscAllowPlainTextPassword=$true
            PSDscAllowDomainUser = $true
            Role = "Secondary"
        },
        @{ 
            Nodename = "DS-DC-1"
            RetryCount = 20
            RetryIntervalSec = 30
            PSDscAllowPlainTextPassword=$true
            PSDscAllowDomainUser = $true
            Role = "Primary"
        }
    )
}