#!/bin/sh
requireddomain=xlc.local
file=updateDNS_go.sh

echo '#!/bin/sh'>$file
echo 'requireddomain=xlc.local'>>$file
echo ''>>$file
echo '# only execute on the primary nic'>>$file
echo 'if [ "$interface" != "eth0" ]'>>$file
echo 'then'>>$file
echo '    return'>>$file
echo 'fi'>>$file

# When you have a new IP, perform nsupdate
echo 'if [ "$reason" = BOUND ] || [ "$reason" = RENEW ] ||'>>$file
echo '   [ "$reason" = REBIND ] || [ "$reason" = REBOOT ]'>>$file
echo 'then'>>$file
echo '   host=`hostname`'>>$file
echo '   nsupdatecmds=/var/tmp/nsupdatecmds'>>$file
echo '     echo "update delete $host.$requireddomain a" > $nsupdatecmds'>>$file
echo '     echo "update add $host.$requireddomain 3600 a $new_ip_address" >> $nsupdatecmds'>>$file
echo '     echo "send" >> $nsupdatecmds'>>$file
echo ''>>$file
echo '     nsupdate $nsupdatecmds'>>$file
echo 'fi'>>$file

chmod +x $file
sudo mv $file /etc/dhcp/dhclient-exit-hooks.d/updateDNS.sh