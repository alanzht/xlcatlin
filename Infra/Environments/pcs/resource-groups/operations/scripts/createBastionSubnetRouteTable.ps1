Param (
    [Parameter (Mandatory = $true)]
    [string] $subscriptionId,
    
    [Parameter (Mandatory = $true)]
    [string] $resourceGroupName,
    
    [Parameter (Mandatory = $true)]
    [string] $resourceLocation,
    
    [Parameter (Mandatory = $true)]
    [string] $vNetName,

    [Parameter (Mandatory = $true)]
    [string] $azureRegion
)

$RouteTableName = "bastion-subnet-route-table"

$VerbosePreference = 'Continue'

### Authenticate with Azure Automation account

$cred = "AzureRunAsConnection"
try {
    # Get the connection "AzureRunAsConnection "
    $servicePrincipalConnection = Get-AutomationConnection -Name $cred

    "Logging in to Azure..."
    Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
}
catch {
    if (!$servicePrincipalConnection) {
        $ErrorMessage = "Connection $cred not found."
        throw $ErrorMessage
    }
    else {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}

Select-AzureRmSubscription -SubscriptionId $subscriptionId

"Subscription Id     : $subscriptionId"
"Resource Group Name : $resourceGroupName"
"Route Table         : $RouteTableName"

### Populate script variables from Azure Automation assets

#$resourceGroupName = Get-AutomationVariable -Name 'virtualNetworkRGName'
#$resourceLocation = Get-AutomationVariable -Name 'virtualNetworkRGLocation'
#$vNetName = Get-AutomationVariable -Name 'virtualNetworkName'
#$azureRegion = Get-AutomationVariable -Name 'azureDatacenterRegions'
$azureRegionSearch = '*' + $azureRegion + '*'

[array]$locations = Get-AzureRmLocation | Where-Object {$_.Location -like $azureRegionSearch}

### Retrieve the nominated virtual network and subnets (excluding the gateway subnet)

$vNet = Get-AzureRmVirtualNetwork `
    -ResourceGroupName $resourceGroupName `
    -Name $vNetName

[array]$subnets = $vnet.Subnets | Where-Object {$_.Name -ne 'GatewaySubnet'} | Select-Object Name

### Create and populate a new array with the IP ranges of each datacenter in the specified location

$ipRanges = @()

foreach ($location in $locations) {
    $ipRanges += Get-MicrosoftAzureDatacenterIPRange -AzureRegion $location.DisplayName
}

$ipRanges = $ipRanges | Sort-Object

### Create DEEPNonHDInsightRouteTable Table
"Building Route table $RouteTableName"
$createDefaultRoute = $TRUE

### Create a new route table if one does not already exist
if ((Get-AzureRmRouteTable -Name $RouteTableName -ResourceGroupName $resourceGroupName) -eq $null) {
    $RouteTable = New-AzureRmRouteTable `
        -Name $RouteTableName `
        -ResourceGroupName $resourceGroupName `
        -Location $resourceLocation
}
### If the route table exists, save as a variable and remove all routing configurations
else {
    "Remove routes"
    $RouteTable = Get-AzureRmRouteTable `
        -Name $RouteTableName `
        -ResourceGroupName $resourceGroupName
    $routeConfigs = Get-AzureRmRouteConfig -RouteTable $RouteTable
    foreach ($config in $routeConfigs) {
        if ($config.Name -eq "DefaultRoute") {
            if ($config.NextHopType -ne 'VirtualNetworkGateway' -or $config.AddressPrefix -ne '0.0.0.0/0') {
                "remove DefaultRoute"
                Remove-AzureRmRouteConfig -RouteTable $RouteTable -Name $config.Name | Out-Null
             }
             else {
                "DefaultRoute ignored..."
                $createDefaultRoute = $FALSE
             }
        }
        else {
            Remove-AzureRmRouteConfig -RouteTable $RouteTable -Name $config.Name | Out-Null
        }
    }
}

### Create a routing configuration for each IP range and give each a descriptive name
$totalIps = $($ipRanges.length)
foreach ($ipRange in $ipRanges) {
    $routeName = ($ipRange.Region.Replace(' ', '').ToLower()) + '-' + $ipRange.Subnet.Replace('/', '-')
    Add-AzureRmRouteConfig `
        -Name $routeName `
        -AddressPrefix $ipRange.Subnet `
        -NextHopType Internet `
        -RouteTable $RouteTable | Out-Null
}

### Add default route for Edge Firewalls

if ($createDefaultRoute -eq $true) {
    "Add DefaultRoute"
    Add-AzureRmRouteConfig `
        -Name 'DefaultRoute' `
        -AddressPrefix 0.0.0.0/0 `
        -NextHopType VirtualNetworkGateway `
        -RouteTable $RouteTable
}


### Include a routing configuration to give direct access to Microsoft's KMS servers for Windows activation
Add-AzureRmRouteConfig `
    -Name 'AzureKMS' `
    -AddressPrefix 23.102.135.246/32 `
    -NextHopType Internet `
    -RouteTable $RouteTable

Add-AzureRmRouteConfig `
    -Name 'digi-crl-ip' `
    -AddressPrefix 72.21.91.29/32 `
    -NextHopType Internet `
    -RouteTable $RouteTable    

### Include Azure authentication and identity ip ranges
[array]$authAndIdentityIps = @("13.67.50.224/29", "13.71.201.64/26", "13.106.4.128/25", "13.75.48.16/29", "13.75.80.16/29", "13.106.56.0/25", "20.190.128.0/18", "23.100.16.168/29", "23.100.32.136/29", "23.100.64.24/29", "23.100.72.32/29", "23.100.80.64/29", "23.100.88.32/29", "23.100.101.112/28", "23.100.104.16/28", "23.100.112.64/29", "23.100.120.64/29", "23.101.5.104/29", "23.101.144.136/29", "23.101.165.168/29", "23.101.181.128/29", "23.101.210.24/29", "23.101.222.240/28", "23.101.224.16/29", "23.101.226.16/28", "40.112.64.16/28", "40.113.192.16/29", "40.114.120.16/29", "40.115.152.16/28", "40.127.67.24/29", "40.126.0.0/18", "52.172.144.16/28", "65.52.1.16/29", "65.52.193.136/29", "65.54.170.128/25", "70.37.154.128/25", "104.40.240.48/28", "104.41.13.120/29", "104.41.216.16/28", "104.42.72.16/29", "104.43.208.16/29", "104.43.240.16/29", "104.44.218.128/25", "104.44.254.128/25", "104.44.255.0/25", "104.45.0.16/28", "104.45.208.104/29", "104.46.112.8/29", "104.46.224.64/28", "104.209.144.16/29", "104.210.48.8/29", "104.210.83.160/29", "104.210.208.16/29", "104.211.16.16/29", "104.211.48.16/29", "104.211.88.16/28", "104.211.216.32/27", "104.215.96.24/29", "104.215.144.64/29", "104.215.184.16/29", "132.245.165.0/25", "134.170.67.0/25", "134.170.116.0/25", "134.170.165.0/25", "134.170.172.128/25", "157.55.45.128/25", "157.55.59.128/25", "157.55.130.0/25", "157.55.145.0/25", "157.55.155.0/25", "157.55.227.192/26", "157.56.53.128/25", "157.56.55.0/25", "157.56.58.0/25", "157.56.151.0/25", "191.232.2.128/25", "191.237.248.32/29", "191.237.252.192/28")
$i = 0
$totalIps = $totalIps + $($authAndIdentityIps.Length)
"Auth and Identity - ip ranges count $($authAndIdentityIps.Length)"
foreach ($ip in $authAndIdentityIps)
{
    $i++
    Add-AzureRmRouteConfig `
        -Name "auth-and-identity-ip$i" `
        -AddressPrefix $ip `
        -NextHopType Internet `
        -RouteTable $RouteTable
}

### Include Azure Red Hat YUM Servers
[array]$RHELYumServers = @("13.91.47.76/32", "40.85.190.91/32", "52.187.75.218/32", "52.174.163.213/32", "52.237.203.198/32")
$i = 0
$totalIps = $totalIps + $($RHELYumServers.Length)
"RHEL Azure YUM Servers - ip ranges count $($RHELYumServers.Length)"
foreach ($ip in $RHELYumServers) {
    $i++
    Add-AzureRmRouteConfig `
        -Name "rhel-azureyum-ip$i" `
        -AddressPrefix $ip `
        -NextHopType Internet `
        -RouteTable $RouteTable
}

### Persist the route table
Set-AzureRmRouteTable -RouteTable $RouteTable

"Total routes $totalIps for route table $RouteTableName"

$associateUdrsToSubnets = @("bastion-subnet")

### Iterate through each subnet in the virtual network
foreach ($subnet in $subnets) {
    if ($associateUdrsToSubnets -Contains $subnet.Name)
    {
        "Apply route table to subnet $($subnet.Name)"

        $vNet = Get-AzureRmVirtualNetwork `
            -ResourceGroupName $resourceGroupName `
            -Name $vNetName

        $forcedTunnelVNet = $vNet.Subnets | Where-Object Name -eq $subnet.Name
        $forcedTunnelVNet.RouteTable = $RouteTable

        ### Update the virtual network with the new subnet configuration
        Set-AzureRmVirtualNetwork -VirtualNetwork $vnet -Verbose
    }
}