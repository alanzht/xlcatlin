param(
    [string] $pcsMainNetRg,
    [string] $pcsVnet,
	[string] $eHubResourceGroup,
    [string] $eHubNameSpaceName,
    
    [string []] $resourceTypes = @("Microsoft.Network/virtualNetworkGateways",
        "Microsoft.RecoveryServices/vaults",
        "Microsoft.Automation/automationAccounts",
        "Microsoft.Network/networkSecurityGroups",
        "Microsoft.Network/publicIPAddresses",
        "Microsoft.Network/loadBalancers",
        "Microsoft.KeyVault/vaults")
)
$subIds =@()
$pcsSubscriptionId=(Get-AzureRmContext).Subscription.Id
$eHubAuthRuleId ="/subscriptions/$pcsSubscriptionId/resourceGroups/$eHubResourceGroup/providers/Microsoft.EventHub/namespaces/$eHubNameSpaceName/authorizationrules/RootManageSharedAccessKey"

Get-AzureRmVirtualNetworkPeering -VirtualNetworkName $pcsVnet -ResourceGroupName $pcsMainNetRg | 
    foreach-object {
        $subIds += $_.RemoteVirtualNetwork.Id.Substring(15, 36)
    }

$subIds += (Get-AzureRmContext).Subscription.Id

foreach ($subId in $subIds) {
    Get-AzureRmSubscription -SubscriptionId $subId `
        | Select-AzureRmSubscription

    #Get resources of the specified types 
    $myResource = Get-AzureRmResource | Where-Object {$resourceTypes -contains $_.ResourceType}
    foreach ($resource in $myResource) {
        Write-Host "Enable diagnostic for resource $($resource.Name)"
        Set-AzureRmDiagnosticSetting -ResourceId $resource.ResourceId -EventHubAuthorizationRuleId $eHubAuthRuleId  -Enabled $true -RetentionEnabled $true -RetentionInDays 90
    }
}