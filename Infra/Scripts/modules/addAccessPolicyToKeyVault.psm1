function Add-accessPolicyToKeyVault{
    param(
        [string]$vaultName
    )


    $spobj = Get-AzureRmADServicePrincipal -SearchString "Backup Management Service"
    Set-AzureRmKeyVaultAccessPolicy -ObjectId $spobj.Id -VaultName $vaultName -PermissionsToKeys get,list,backup -PermissionsToSecrets get,list,backup
}

Export-ModuleMember -Function 'Add-accessPolicyToKeyVault'