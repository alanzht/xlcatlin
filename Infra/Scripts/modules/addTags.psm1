function Add-TagsToAllResources{
    param(
        [PSObject] $rg,
        [string] $tagsJson
    )

    $tags = ($tagsJson | ConvertFrom-Json).Tags
    $tagsToAdd = @{}

    foreach ($tag in $tags)
    {
        $tagsToAdd.Add($tag.Name, $(Get-TagValue -Rg $rg -Tag $tag) )
    }

    $rgRole = $tagsToAdd["Role"]
    #$tagsToAdd | Format-Table
    try{
        Set-AzureRmResourceGroup -Tag $tagsToAdd -Name $rg.ResourceGroupName -ErrorAction Stop # set tags for the resource group
    }catch{
        $ErrorMessage = $_.Exception.Message
        Write-Warning "Error : $ErrorMessage"
    }
    foreach ($resource in $(Get-AzureRmResource -ResourceGroupName $rg.ResourceGroupName)) {
        try{
            $tagsToAdd["ResourceType"] = $resource.ResourceType.Substring($resource.ResourceType.LastIndexOf('/') + 1)
            $tagsToAdd["Region"] = $resource.Location
            # Resource group role is applied to all the respiurces except VM resources
            $tagsToAdd["Role"] = $rgRole
            [bool] $vmResource = $($resource.ResourceType -eq 'Microsoft.Compute/virtualMachines')
            [int] $indexOf = $resource.Name.IndexOf('vm-')

            if ($vmResource -and $($indexOf -ne -1)) {
                [string] $roleValue = $resource.Name.Substring($indexOf + 3)
                [int] $valueLength = $(if ($($roleValue.IndexOf('-') -ne -1)) { $roleValue.IndexOf('-')} else { $roleValue.Length } )
                $tagsToAdd["Role"] = $roleValue.Substring(0, $valueLength)
            }
            else {
                if ($vmResource) {
                    $tagsToAdd["Role"] = 'unknown'
                }
            }
            #$tagsToAdd | Format-Table
            Set-AzureRmResource -ResourceId $resource.ResourceId -Tag $tagsToAdd -ErrorAction Stop -Force
        }catch{
            $ErrorMessage = $_.Exception.Message
            Write-Warning "Error : $ErrorMessage"
        }
    }
}

function Add-TagsToAllGroupsAndResources {
    param (
        [string] $tagsJson
    )

    $groups = Get-AzureRmResourceGroup
    foreach ($group in $groups) {
        $group
        Add-TagsToAllResources -rg $group -tagsJson $tagsJson
    }
}

function Get-TagValue {
    param(
        [PSObject] $rg,
        [PSObject] $tag
    )

    $value = $tag.Default

    switch ($tag.Name) {
        "Region" {
            $value = $rg.Location
            break
        }
        "ResourceType" {
            $value = "resourceGroup"
            break
        }
        "Role" {
            if ($rg.ResourceGroupName.IndexOf('rg-') -ne -1) {
                $value = $rg.ResourceGroupName.Substring($rg.ResourceGroupName.IndexOf('rg-') + 3)
            }
            else {
                $value = 'unknown'
            }
            break
        }
        Default {
            break
        }
    }

    return $value
}

Export-ModuleMember -Function 'Add-Tags*'