function Disable-diskEncryption{
    param(
        [string]$resourceGroup,
        [string]$vmName
    )


    Disable-AzureRMVMDiskEncryption -ResourceGroupName $resourceGroup `
                                    -VMName $vmName `
                                    -VolumeType "All" `
                                    -Force
}

Export-ModuleMember -Function 'Disable-diskEncryption'