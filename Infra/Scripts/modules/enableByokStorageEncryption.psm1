function Enable-ByokStorageEncryption {
    param (
        [string]$resourceGroup,
        [string]$keyVaultResourceGroup,
        [string]$storageAccountName,
        [string]$keyVaultName,
        [string]$encryptionKeyName
    )

    process {
        $storageAccount = Get-AzureRmStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName
        Write-Information "Storage account encryption key source -$($storageAccount.Encryption.KeySource)"
        Write-Information "Change encryption key source to Microsoft.Keyvault"
        # Assign Identity to the storage account
        $storageAccount = Set-AzureRmStorageAccount -Name $storageAccountName -ResourceGroupName $resourceGroup -AssignIdentity
        Start-Sleep -Seconds 30
        
        if ($keyVaultName){
            $keyVault = Get-AzureRmKeyVault -VaultName $keyVaultName 
        }elseif ($keyVaultResourceGroup){
            $allKeyVaults = Get-AzureRmKeyVault -ResourceGroupName $keyVaultResourceGroup 
            $keyVault = Get-AzureRmKeyVault -VaultName $allKeyVaults[0].VaultName 
        }else {
            $allKeyVaults = Get-AzureRmKeyVault 
            $keyVault = Get-AzureRmKeyVault -VaultName $allKeyVaults[0].VaultName 
        }

        $key = Get-AzureKeyVaultKey -VaultName $keyVault.VaultName -Name $encryptionKeyName 

        if ($key -eq $null) {
            # Add key to the Key Vault.
            $key = Add-AzureKeyVaultKey `
                -VaultName $keyVault.VaultName `
                -Name $encryptionKeyName -KeyOps encrypt, decrypt, wrapKey, unwrapKey `
                -Destination 'Software'
                
        }

        Set-AzureRmKeyVaultAccessPolicy `
                -VaultName $keyVault.VaultName `
                -ObjectId $storageAccount.Identity.PrincipalId `
                -PermissionsToKeys wrapKey, unwrapKey, get `
                -BypassObjectIdValidation
            
        
        # assign key to storage account.
        Set-AzureRmStorageAccount `
                -ResourceGroupName $storageAccount.ResourceGroupName `
                -AccountName $storageAccount.StorageAccountName `
                -KeyName $key.Name `
                -KeyVersion $key.Version `
                -KeyVaultUri $keyVault.VaultUri `
                -KeyvaultEncryption
    }
}


Export-ModuleMember -Function 'Enable-ByokStorageEncryption'
