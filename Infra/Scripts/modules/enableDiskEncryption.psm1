function Enable-diskEncryption{
    param(
        [string]$resourceGroup,
        [string]$keyVaultResourceGroup,
        [string]$vmName,
        [string]$keyVaultName,
        [string]$keyEncryptionName
    )

        $encryption_status = Get-AzureRmVmDiskEncryptionStatus -ResourceGroupName $resourceGroup `
                        -VMName $vmName -ErrorAction Ignore

        if ($encryption_status.OsVolumeEncrypted -ne "Encrypted"){

            if ($keyVaultName){
                $keyVault = Get-AzureRmKeyVault -VaultName $keyVaultName
            }elseif ($keyVaultResourceGroup){
                $allKeyVaults = Get-AzureRmKeyVault -ResourceGroupName $keyVaultResourceGroup
                $keyVault = Get-AzureRmKeyVault -VaultName $allKeyVaults[0].VaultName 
            }else {
                $allKeyVaults = Get-AzureRmKeyVault
                $keyVault = Get-AzureRmKeyVault -VaultName $allKeyVaults[0].VaultName
            }
            
            $diskEncryptionKeyVaultUrl = $keyVault.VaultUri
            $keyVaultResourceId = $keyVault.ResourceId
            $keyVaultName = $keyVault.VaultName

            $key = Add-AzureKeyVaultKey `
                    -VaultName $keyVaultName `
                    -Name $keyEncryptionName `
                    -Destination 'Software'

            Set-AzureRmVMDiskEncryptionExtension `
                    -ResourceGroupName $resourceGroup `
                    -VMName $vmName `
                    -DiskEncryptionKeyVaultUrl $diskEncryptionKeyVaultUrl `
                    -DiskEncryptionKeyVaultId $keyVaultResourceId `
                    -KeyEncryptionKeyVaultId $keyVaultResourceId `
                    -KeyEncryptionKeyUrl $key.Id `
                    -VolumeType "All" `
                    -Force `
                    -SkipVmBackup
        }      

    
}

Export-ModuleMember -Function 'Enable-diskEncryption'