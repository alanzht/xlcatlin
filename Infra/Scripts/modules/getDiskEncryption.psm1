function Get-diskEncryption{
    param(
        [string]$resourceGroup,
        [string]$vmName
    )


    Get-AzureRmVmDiskEncryptionStatus   -ResourceGroupName $resourceGroup `
                                        -VMName $vmName
}

Export-ModuleMember -Function 'Get-diskEncryption'