function Get-FromParamFile{
    param(
        [string]$parameterFolder
    )

    $wrapper = '{ }' | ConvertFrom-Json

    $files = Get-ChildItem -Path $parameterFolder -Filter *.json
    foreach($file in $files){
        $Jsonfile = Get-Content -Raw -Path $parameterFolder$file | ConvertFrom-Json
        foreach($Property in $Jsonfile | Get-Member -type NoteProperty, Property){
            $wrapper | Add-Member -MemberType NoteProperty -name $Property.Name -Value $Jsonfile.$($Property.Name)
        }
        
    }

    return $wrapper
}

Export-ModuleMember -Function 'Get-FromParamFile'