function Get-vnetID{
    param(
        [string]$groupName,
        [string]$vnetName
    )
    $VNET_ID=(Get-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $groupName -ErrorAction SilentlyContinue).Id

    return $VNET_ID
}
Export-ModuleMember -Function 'Get-vnetID'
