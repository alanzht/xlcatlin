function New-BuildParamFile
{
    [CmdletBinding()]
    param(
        [string[]]$parameterFolders,
        [string]$outputFile,
        [PSObject]$replaceTokens
    )
    Write-Verbose "Parameter Values: "
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }

    $wrapper = '{
        "$schema": "http://schema.management.azure.com/schemas/2015-01-01/deploymentParameters.json#",
        "contentVersion": "1.0.0.0", "parameters": { "global":{ "value": {} } } 
    }' | ConvertFrom-Json

    write-host ""

    foreach ($ParamFolder in $parameterFolders)
    {
        write-host "Parameter folder :  $ParamFolder"
        $files = Get-ChildItem -Path "$ParamFolder\*" -Filter "*.json" -Exclude "_*", "Settings.json"    
        foreach($file in $files){
            Write-Host " - $file"
            $Jsonfile = Get-Content -Raw -Path $file.fullname | ConvertFrom-Json
            foreach($Property in $Jsonfile | Get-Member -type NoteProperty, Property){
                $wrapper.parameters.global.value | Add-Member -MemberType NoteProperty -name $Property.Name -Value $Jsonfile.$($Property.Name) -Force
            }            
        }
    }   

    Write-verbose "**** Original Tokens:"
    foreach ($key in $($replaceTokens.keys))
    {
        Write-verbose "Key: $key, Value: $($replaceTokens[$key])"         
    }
    Write-verbose "****"
    Write-verbose ""
    Write-verbose "Replace takens:"

    foreach($key in $($replaceTokens.keys))
    {
        Write-verbose "Key: $key, Value: $($replaceTokens[$key]), Type: $($replaceTokens[$key].GetType() )"      

        if($replaceTokens[$key] -match "\[.*\]")
        {
            Write-verbose "  Found Object reference token []"
            $value = (($replaceTokens[$key]).replace('[','').replace(']','')).split('.')
            $innerObj = $wrapper.parameters.global.value
            Write-Verbose  "  Ref: $value"
            foreach ($property in $value)
            {
                $innerObj = $innerObj.$property
            }
            $replaceTokens[$key] = $innerObj
            Write-Verbose "   Val: $innerObj"
        }
        elseif ($replaceTokens[$key] -match "^\(.*\)$")
        {
            Write-verbose "  Found Code token ()"
            $value = Invoke-Expression -Command "$($replaceTokens[$key])"
            $replaceTokens[$key] = $value
        }
        elseif ( $replaceTokens[$key].GetType() -eq [Boolean])
        {
            Write-verbose "  Found Boolean type"
        }
        elseif ( $replaceTokens[$key].GetType() -eq [System.Management.Automation.PSCustomObject]) {
            Write-verbose "  Found PSCustomObject type"
        }
        else {
            Write-verbose "  No token found"
        }
        Write-verbose ""
    }

    Write-verbose "**** New Tokens"
    foreach ($key in $($replaceTokens.keys)) {
        Write-Host "Key: $key, Value: $($replaceTokens[$key])"         
    }
    Write-verbose "****"
    Write-verbose ""
    
    $global = $wrapper | ConvertTo-Json -Depth 100  | % { [System.Text.RegularExpressions.Regex]::Unescape($_) } | % {$_.replace("\", "\\")} 

    if ($replaceTokens){
        foreach($token in $replaceTokens.GetEnumerator()){
            if ( $token.Value.GetType() -eq [Boolean] )
            {          
                Write-Verbose "Process Boolean value $($token.Name)"      
                $global = $global.replace( "`"$($token.Name)`"", $token.Value.ToString().ToLower()) 
            }         
            elseif ( $token.Value.GetType() -eq [System.Management.Automation.PSCustomObject] )
            {
                Write-Verbose "Process PSCustomObject value $($token.Name)"  
                $global = $global.replace( "`"$($token.Name)`"", ($token.Value | ConvertTo-Json -Depth 100)  ) 
            }  
            elseif ( $token.Value -match "^\d+$" -and $token.Value.GetType() -ne [string] )
            {
                Write-Verbose "Process number value $($token.Name)"  
                $global = $global.replace( "`"$($token.Name)`"", $token.Value) 
            }
            else 
            {
                Write-Verbose "Process text value $($token.Name)"
                $global = $global.replace($token.Name,$token.Value) 
            }            
        }
    }  

    if(Test-Path -Path $outputFile){
        Clear-Content $outputFile
    }
    $global | Add-Content $outputFile    
}

Export-ModuleMember -Function 'New-BuildParamFile'