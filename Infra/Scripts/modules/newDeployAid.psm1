function New-deployAid{
    param(
        [string]$location,
        [string]$groupName,
        [string]$storageAccountName,
        [Array]$folderPaths
    )

    # Create resouce group
    New-AzureRmResourceGroup -Name $groupName -Location $location -Force;

    $deployAidStorageAccount = Get-AzureRmStorageAccount -AccountName $storageAccountName -ResourceGroupName $groupName -ErrorAction SilentlyContinue

    # Create storage account
    if(!$deployAidStorageAccount){
        $deployAidStorageAccount = New-AzureRmStorageAccount -ResourceGroupName $groupName `
            -Name $storageAccountName `
            -Location $location `
            -SkuName Standard_LRS `
            -Kind StorageV2 
        Write-Host "[Finish] creating $storageAccountName storage account in $location location"
    }else{
        Write-Host "$storageAccountName storage account in $location location already exists, skipping creation"
    }

    $ctx = $deployAidStorageAccount.Context


    foreach($folder in $folderPaths){

        $container_name=$folder.container
        $folder_path=$folder.path
        $folder_compress=$folder.compress

        $container = Get-AzureStorageContainer -Name $container_name -Context $ctx -ErrorAction SilentlyContinue

        # Create container
        if(!$container){
            New-AzureStorageContainer -Name $container_name -Context $ctx -Permission Off
            Write-Host "[Finish] creating container $container_name in storage account $storageAccountName"
        }else{
            Write-Host "$container_name container in $storageAccountName storage account already exists, skipping creation"
        }
        
        # Upload files
        if($folder_compress){
            $folder_name = Split-Path (Split-Path "$folder_path" -Leaf ) -Leaf
            $file_compress = "$folder_name.zip"
            Compress-Archive -Path "$folder_path\*" -CompressionLevel Fastest -DestinationPath $file_compress
            Set-AzureStorageBlobContent -Container $container_name -File $file_compress -Context $ctx -Force
            Remove-Item -path "$file_compress"
        }else{
            Get-ChildItem -Path $folder_path -File -Recurse | Set-AzureStorageBlobContent -Container $container_name -Context $ctx -Force
        }
        
    }
}

Export-ModuleMember -Function 'New-deployAid'