function New-key{
    param(
        [string]$keyVault,
        [string]$keyName
    )

    $expires = (Get-Date).AddYears(2).ToUniversalTime()

    $key = Add-AzureKeyVaultKey -VaultName $keyVault `
                                -Name $keyName `
                                -Destination 'Software' `
                                -Expires $expires

    return $key.id
}

Export-ModuleMember -Function 'New-key'