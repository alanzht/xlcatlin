function New-vnetPeering{
  param(
      [string]$groupName,
      [string]$vnetName,
      [string]$remoteVnetId,
      [string]$name = "remotePeer",
      [switch]$UseRemoteGateways,
      [switch]$AllowGatewayTransit
  )

  Write-Host "Create new peering between local vnet name : $vnetName to remote vnet ID : $remoteVnetId"
  Write-Host "UseRemoteGateways : $UseRemoteGateways , AllowGatewayTransit : $AllowGatewayTransit"

  $localVnet=Get-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $groupName 

  Add-AzureRmVirtualNetworkPeering `
    -Name $name `
    -VirtualNetwork $localVnet `
    -RemoteVirtualNetworkId $remoteVnetId `
    -AllowForwardedTraffic `
    -UseRemoteGateways:$UseRemoteGateways `
    -AllowGatewayTransit:$AllowGatewayTransit

}
Export-ModuleMember -Function 'New-vnetPeering'