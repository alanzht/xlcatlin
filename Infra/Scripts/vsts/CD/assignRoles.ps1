<#
    Description: This file is used to assign custom roles to Azure AD Group
#>

param (
    [switch] $remove,
    [string] $env = 'poc',
    [string] $platform = 'pcs'
)

Set-Location -Path ($PSScriptRoot)

# $context = Get-AzureRmContext

# $templateDir = "..\..\parameters\policies"

$roleAssignments = (Get-Content "..\..\parameters\azure-ad\group-roles\$env\$platform.roleAssignment.json" | ConvertFrom-Json).RoleAssignments

foreach ($roleAssignment in $roleAssignments)
{
    if($remove) {
        Write-Host "Remove role assignment objectId $($roleAssignment.GroupId) role definition $($roleAssignment.RoleDefinitionName)"
        Remove-AzureRmRoleAssignment -ObjectId $roleAssignment.GroupId -RoleDefinitionName $roleAssignment.RoleDefinitionName
    }
    else {
        $ra = Get-AzureRmRoleAssignment -ObjectId $roleAssignment.GroupId -RoleDefinitionName $roleAssignment.RoleDefinitionName
        if ($null -eq $ra)
        {
            if ($null -ne $($roleAssignment.Scope)) {
                Write-Host "Create new role assignment scope $($roleAssignment.Scope)"
                New-AzureRmRoleAssignment -ObjectId $roleAssignment.GroupId `
                    -RoleDefinitionName $roleAssignment.RoleDefinitionName `
                    -Scope $roleAssignment.Scope
            }
            elseif ($null -ne $($roleAssignment.ResourceGroupName)) {
                Write-Host "Create new role assignment resource group $($roleAssignment.ResourceGroupName)"
                New-AzureRmRoleAssignment -ObjectId $roleAssignment.GroupId `
                    -RoleDefinitionName $roleAssignment.RoleDefinitionName `
                    -ResourceGroupName $roleAssignment.ResourceGroupName
            }
        }
        else {
            Write-Host "Role Assignment already exists."
            $ra
        }
    }
}
 