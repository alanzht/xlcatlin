param(
    [string]$keyVault,
    [parameter(Mandatory=$true)][string[]]$ObjectIds
)

foreach ($ObjectId in $ObjectIds){
    Set-AzureRmKeyVaultAccessPolicy `
            -ObjectId $ObjectId `
            -VaultName $keyVault `
            -PermissionsToKeys encrypt,decrypt,UnwrapKey,WrapKey,Verify,sign,create,import,get,list,backup,update,recover,restore,delete,purge `
            -PermissionsToSecrets get,list,backup,delete,backup,restore `
            -PermissionsToCertificates get,list,backup `
            -BypassObjectIdValidation
}