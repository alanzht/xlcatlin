<#
    Description: This file is used to create policies for subscriptions
#>

param (
    [switch] $addNotScope,
    [string] $PoliciesFolder = "..\..\parameters\policies\default"
)

Set-Location -Path ($PSScriptRoot)

[string] $subscriptionId = (Get-AzureRmContext).Subscription.Id

Get-ChildItem -Path $PoliciesFolder | ForEach-Object {
    $policyFileFullPath = $_.FullName
    $policyObject = (Get-Content $policyFileFullPath | ConvertFrom-Json)
    Write-Host "Policy file: $policyFileFullPath"
    $policy = Get-AzureRmPolicyDefinition -BuiltIn | Where-Object {$_.Properties.DisplayName -eq $policyObject.name}
    #$Policy = Get-AzureRmPolicyDefinition | Where-Object {$_.Name -eq $policyObject.definitionId}
    	
	$notScopeList = @()
    foreach($resourceType in $policyObject.notScope){
        $resources = Get-AzureRmResource -ResourceType $resourceType
        foreach ($resource in $resources){
            $notScopeList += $resource.ResourceId
        }
    }
  
    if ($notScopeList.length -gt 0 -and $addNotScope -eq $true ) {
        "Following resources will be added to NotScope"
        $notScopeList
        New-AzureRmPolicyAssignment -Name $policyObject.name `
                                    -DisplayName $policyObject.displayName `
                                    -PolicyDefinition $policy `
                                    -Scope "/subscriptions/$SubscriptionId" `
                                    -NotScope $notScopeList `
                                    -PolicyParameter ( $policyObject.parameters | convertTo-Json) `
                                    -Description $policyObject.description
    }
    else {
        New-AzureRmPolicyAssignment -Name $policyObject.name `
                                    -DisplayName $policyObject.displayName `
                                    -PolicyDefinition $policy `
                                    -Scope "/subscriptions/$SubscriptionId" `
                                    -PolicyParameter ( $policyObject.parameters | convertTo-Json) `
                                    -Description $policyObject.description
    }
    <##>
}
