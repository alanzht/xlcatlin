<#
    Description: This file is used to create roles for RBAC
#>

param (
    [switch] $remove,
    [parameter(Mandatory = $False)][string[]] $subscriptionIds
)

Set-Location -Path ($PSScriptRoot)
$currentContext = Get-AzureRmContext
$localDir = "local_role_templates"
$templateDir = "..\..\parameters\role_templates"
$prefix = "XLC"

if (!(Test-Path -Path $localDir)) {
    New-Item -ItemType Directory -Path $localDir
    Write-Output "Directory $localDir created"
}

Get-ChildItem -Path $templateDir | ForEach-Object {
    $policyTemplate = (Get-Content $_.FullName | ConvertFrom-Json)
    $policyTemplate.Name = "$prefix-$($policyTemplate.Name)"
    Set-Content "$localDir\$($policyTemplate.Name).json" (ConvertTo-Json -InputObject $policyTemplate) -Force
}

Get-ChildItem -Path $localDir | ForEach-Object {
    (Get-Content $_.FullName).replace('xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx', $($currentContext.Subscription.Id)) `
        | Set-Content $_.FullName -Force
}

Get-ChildItem -Path $localDir | ForEach-Object {
    try {
      
        $policyTemplateFullName = $_.FullName
        $policyTemplate = (Get-Content $policyTemplateFullName | ConvertFrom-Json)
        $policyTemplateName = $policyTemplate.Name
        $policyTemplateFullName
        
        if ($remove -eq $True) {
            Write-Output "Remove role $policyTemplateName"
            Remove-AzureRmRoleDefinition -Name $policyTemplateName -Force
        }
        else {
            [bool]$updateRoleDef = $false
            $roleDef = Get-AzureRmRoleDefinition -Name $policyTemplateName
    
            if (($roleDef).Name -ne $policyTemplateName) {
                Write-Output "Create new role $policyTemplateName"
                $roleDef = New-AzureRmRoleDefinition -InputFile $policyTemplateFullName
            }
            else {
                Write-Output "Update role $policyTemplateName - $($roleDef.Id)"
                # Set id in json template to match role def id
                (Get-Content $_.FullName).replace('#GUIDID#', $($roleDef.Id)) | Set-Content $_.FullName -Force
                # Update json role template file changes
                $roleDef = Set-AzureRmRoleDefinition -InputFile $policyTemplateFullName
            }

            foreach ($subscriptionId in $subscriptionIds) {
                if ($roleDef.AssignableScopes -notcontains "/subscriptions/$subscriptionId") {
                    $roleDef.AssignableScopes.Add("/subscriptions/$subscriptionId")
                    $updateRoleDef = $True
                }
            }
     
            if ($updateRoleDef) {
                $roleDef = Set-AzureRmRoleDefinition -Role $roleDef
            }

            $roleDef
        }
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        Write-Output "Error creating/updating RBAC Role Template $PolicyTemplateName"
        Write-Error "Error: $ErrorMessage"
    }
}

Remove-Item $localDir -Force -Recurse