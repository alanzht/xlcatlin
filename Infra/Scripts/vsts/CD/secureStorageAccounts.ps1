[array]$vnets = Get-AzureRmVirtualNetwork
$subnetIds = @()

# Enable service endpoint for Storage account for virtual network and subnets.
foreach($vnet in $vnets) {
    foreach ($subnet in $vnet.Subnets) {
        Set-AzureRmVirtualNetworkSubnetConfig -Name $subnet.Name `
            -VirtualNetwork $vnet `
            -AddressPrefix $subnet.AddressPrefix `
            -ServiceEndpoint "Microsoft.Storage" | Set-AzureRmVirtualNetwork
        
        $subnetIds += $subnet.Id
    }
}

# Add a network rule for a Virtual Network and subnets for all the storage account under current subscription. 
[array]$storageAccounts = Get-AzureRmStorageAccount
foreach ($storageAccount in $storageAccounts) {
    $networkRuleSet = (Get-AzureRmStorageAccountNetworkRuleSet -ResourceGroupName $storageAccount.ResourceGroupName `
                            -StorageAccountName $storageAccount.StorageAccountName)
<##>
    if ($networkRuleSet.VirtualNetworkRules.Length -gt 0) {
        "Remove existing virtual network rules"
        Remove-AzureRmStorageAccountNetworkRule -ResourceGroupName $storageAccount.ResourceGroupName `
            -Name $storageAccount.StorageAccountName `
            -VirtualNetworkRule $networkRuleSet.VirtualNetworkRules
    }

    if ($networkRuleSet.DefaultAction -eq "Allow") {
        Update-AzureRmStorageAccountNetworkRuleSet -ResourceGroupName $storageAccount.ResourceGroupName `
            -StorageAccountName $storageAccount.StorageAccountName `
            -DefaultAction "Deny"
    }

    Add-AzureRmStorageAccountNetworkRule -ResourceGroupName $storageAccount.ResourceGroupName `
            -Name $storageAccount.StorageAccountName `
            -VirtualNetworkResourceId $subnetIds
<##>
}