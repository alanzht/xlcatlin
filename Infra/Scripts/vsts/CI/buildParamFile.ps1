[CmdletBinding()]
param(
    [parameter(Mandatory = $true)]
    [string]$ParameterFolder,

    [parameter(Mandatory = $true)]
    [string]$ParameterFolderCommon,

    [parameter(Mandatory = $true)]
    [string]$OutputFile
)
if ( $null -ne $env:BUILD_REPOSITORY_LOCALPATH )
{
    Set-Location -Path ($env:BUILD_REPOSITORY_LOCALPATH)
}

Import-module -Name .\Scripts\Modules\newBuildParamFile


#$ParameterFolder = "$CurrentFolder\$Folder\Parameters\"
#$OutputFile = "$CurrentFolder\$Folder\Parameters.json"

$replaceTokens = @{}

$Settings           = Get-Content -Raw -Path "$ParameterFolder\Settings.json" | ConvertFrom-Json
$DynamicParameters  = "{}" | ConvertFrom-Json

#$DynamicParameters | Add-Member -MemberType NoteProperty -Name "automationAccount" -value $null

if ($Settings.runbooks)
{
    Write-Verbose "Runbooks found in Settings.json"
    $DynamicParameters | Add-Member -MemberType NoteProperty -Name "automationAccount" -value $null
    $FoundRunBooks = $true
}

if ($Settings.routetables)
{
    Write-Verbose "Route Tables found in Settings.json"
    $DynamicParameters | Add-Member -MemberType NoteProperty -Name "routetables" -value @()
    $FoundRouteTables = $true
}

if ($Settings.subnets)
{
    Write-Verbose "Subnets found in Settings.json"
    $DynamicParameters | Add-Member -MemberType NoteProperty -Name "subnets" -value @()
    $FoundSubnets = $true
}

 Write-Verbose "Created Dynamic object for parameters"
 
 if ( `
        $FoundRunBooks `
        -and `
        (test-path -Path "$ParameterFolderCommon\_automationAccount.json") `
        -and `
        (test-path -Path "$ParameterFolderCommon\_runbook.json") `
 )
 {
    Write-Verbose "Found AutomationAccount & RunBook templates"
    $AutomationAccount  =    Get-Content -Raw -Path "$ParameterFolderCommon\_automationAccount.json" | ConvertFrom-Json
    $RunBookTemplate    =    Get-Content -Raw -Path "$ParameterFolderCommon\_runbook.json"

    $DynamicParameters.automationAccount = $AutomationAccount.automationAccount

    $i = 0
    foreach ($book in $Settings.runbooks) {    
        $NewBook = $RunBookTemplate | ConvertFrom-Json        
        $NewBook.book.name = $NewBook.book.name.replace("[RB-Name-RB]", "[RB-Name$i-RB]")
        $NewBook.book.description = $NewBook.book.description.replace("[RB-Description-RB]", "[RB-Description$i-RB]")
        $NewBook.book.uri = $NewBook.book.uri.replace("[RB-Script-RB]", "[RB-Script$i-RB]")

        $NewBook.job.name = $NewBook.job.name.replace("[RB-Name-RB]", "[RB-Name$i-RB]")

        $NewBook.scheduler.name = $NewBook.scheduler.name.replace("[RB-Name-RB]", "[RB-Name$i-RB]")
        $NewBook.scheduler.description = $NewBook.scheduler.description.replace("[RB-Description-RB]", "[RB-Description$i-RB]")

        $DynamicParameters.automationAccount.runbooks += $NewBook            

        $replaceTokens["[RB-Name$i-RB]"] = $book.name
        $replaceTokens["[RB-Description$i-RB]"] = $book.description
        $replaceTokens["[RB-Script$i-RB]"] = $book.script
        $i++
    }
 }  

Write-Verbose "Found $($Settings.routetables.count) routetables in settings.json"
foreach ($item in $Settings.routetables) { $DynamicParameters.routetables += $item }

Write-Verbose "Found $($Settings.subnets.count) subnets in settings.json"
foreach ($item in $Settings.subnets) { $DynamicParameters.subnets += $item }

$TmpParameterFolder = $ParameterFolder + "\Parameters\"

if ( !(Test-path -Path $TmpParameterFolder) )
{
    Write-Verbose "Info: Create tmp parameter folder: $TmpParameterFolder"
    New-Item -Path "$TmpParameterFolder" -ItemType Directory -Force  | Out-Null
}

$DynamicParameters | ConvertTo-Json -Depth 100 | Set-Content -Path "$TmpParameterFolder\DynamicParameters.json" -Force

if (Test-Path -Path "$OutputFile" ) 
{
    Write-Verbose "Output file exists, deleting..."
    Remove-Item -Path $OutputFile -Force
}

$Settings.tokens.psobject.properties | ForEach-Object { $replaceTokens[$_.Name] = $_.Value }

Write-Verbose "Token list:"
$replaceTokens
Write-Verbose ""

New-BuildParamFile -parameterFolders $ParameterFolder, $ParameterFolderCommon, $TmpParameterFolder -outputFile $outputFile -replaceTokens $replaceTokens -verbose

#Get-Content -Path "$OutputFile" | ConvertFrom-Json | ConvertTo-Json -Depth 100 | Set-Content -Path "$OutputFile.EasyReading.json"

if ( Test-path -Path $TmpParameterFolder ) {
    Write-Verbose "Info: Remove parameter folder: $TmpParameterFolder"
    Remove-Item -Path "$TmpParameterFolder" -Recurse -Force -Confirm:$false | Out-Null
}

Remove-module newBuildParamFile