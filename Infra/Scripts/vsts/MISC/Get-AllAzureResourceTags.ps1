﻿<#
    Author: Alex Skinner
    Contact: alex.skinner@contractor.xlcatlin.com | alex.skinner2@hpe.com
    Description: This script retrieves all resources from Azure and outputs them along with their tags into a CSV file.
    Version: 1.0
#>

# *****************************************************************************************************************
# IMPORTANT
# 
# To use this script you need to have installed the AzureRM module. This can be acheived using the command:
#
# Install-Module AzureRm
# 
# N.B. This must be run as an administrator on your device.
# 
# Before running this script, you may need to run the cmdlet 'Login-AzureRmAccount' and login to the XL 
# Catlin Azure tenants.
# *****************************************************************************************************************

# Creating the filename in the format `deep tag report ddmmyy hhmm.csv` as requested
$now = Get-Date -format "dd MMM yyyy HHmm"
$outputFile = "deep tag report $($now).csv"
"" | Set-Content $outputFile

# Retrieve all subscriptions
$subscriptions = Get-AzureRmSubscription

# The property names used in the resource object $objResource
$propSubName = "SubscriptionName"
$propResGroupName = "ResourceGroupName"
$propResName = "ResourceName"
$locationName = "Location"
$propResType = "ResourceType"
$propResId = "ResourceId"

$resources = @()

$index = 0
foreach ($subscription in $subscriptions){
    Write-Host "Using subscription $($subscription.Name) with id:$($subscription.Id)" -BackgroundColor Gray -ForegroundColor Yellow
    $sub = Select-AzureRmSubscription $subscription.Id
    $resourceList = Get-AzureRmResource
    foreach ($resource in $resourceList){
        # Create a new object for each resource found and add necessary information to it.
        Write-Host "`tProcessing resource: $($resource.Name)" -ForegroundColor Cyan
        $objResource = New-Object -TypeName PSObject
        $objResource | Add-Member -MemberType NoteProperty –Name $propSubName –Value $subscription.Name -Force
        $objResource | Add-Member -MemberType NoteProperty -Name $propResGroupName -Value $resource.ResourceGroupName -Force
        $objResource | Add-Member -MemberType NoteProperty -Name $propResName -Value $resource.Name -Force
        $objResource | Add-Member -MemberType NoteProperty -Name $locationName -Value $resource.Location -Force
        $objResource | Add-Member -MemberType NoteProperty -Name $propResType -Value $resource.Type -Force
        $objResource | Add-Member -MemberType NoteProperty -Name $propResId -Value $resource.Id -Force
        $tags = $resource.Tags
        foreach ($key in $resource.Tags.Keys) {
            $value = $resource.tags[$key]
            $objResource | Add-Member -MemberType NoteProperty -Name "Tag:$($key)" -Value $value -Force
        }
        $resources += $objResource
     }
     $index++
}
# Extract all possible headings (as tag names are dynamic)
$Props=$resources | ForEach{$_.PSObject.Properties} | Select -Expand Name -Unique
$Props|Where{$_ -notin $resources[0].psobject.properties.name}|ForEach{Add-Member -InputObject $resources[0] -NotePropertyName $_ -NotePropertyValue $null}

# Write all resources and tags to a CSV file.
$resources | Export-Csv $outputFile -NoTypeInformation

Write-Host "Output saved too: $((Get-Item -Path ".\").FullName)\$($outputFile)" -BackgroundColor green -ForegroundColor Black