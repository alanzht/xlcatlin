﻿$outputFile = 'vmList.txt'
"" | Set-Content $outputFile
$subscriptions = Get-AzureRmSubscription

foreach ($subscription in $subscriptions){
    $out = "Using subscription $($subscription.Name) with id:$($subscription.Id)"
    $out | Add-Content $outputFile
    Write-Host $out
    $sub = Select-AzureRmSubscription $subscription.Id
    $vms = Get-AzureRmResource -ResourceType "Microsoft.Compute/virtualMachines"
    foreach ($vm in $vms){
        $out = "Found a vm $($vm.Name) in resource group:$($vm.ResourceGroupName)"
        $out | Add-Content $outputFile
        Write-Host $out
    }
}