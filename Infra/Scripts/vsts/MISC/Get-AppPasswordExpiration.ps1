param (
    [switch] $outputToCsvFile,    
    [switch] $listAll,
    [parameter(Mandatory = $False)][datetime] $expiryDate = (get-date).AddDays(7).Date
)

$results = @()

Get-AzureADApplication | 
    ForEach-Object {
    $app = $_
    $owner = Get-AzureADApplicationOwner -ObjectId $app.ObjectID -Top 1

    Get-AzureADApplicationPasswordCredential -ObjectId $app.ObjectID | 
        ForEach-Object { 
        $results += [PSCustomObject] @{
            CredentialType = "PasswordCredentials"
            DisplayName    = $app.DisplayName; 
            ExpiryDate     = $_.EndDate;
            StartDate      = $_.StartDate;
            KeyID          = $_.KeyId;
            Type           = 'NA';
            Usage          = 'NA';
            Owners         = $owner.UserPrincipalName;
        }
    }
                                  
    Get-AzureADApplicationKeyCredential -ObjectId $app.ObjectId | 
        ForEach-Object { 
        $results += [PSCustomObject] @{
            CredentialType = "KeyCredentials"
            DisplayName    = $app.DisplayName; 
            ExpiryDate     = $_.EndDate;
            StartDate      = $_.StartDate;
            KeyID          = $_.KeyId;
            Type           = $_.Type;
            Usage          = $_.Usage;
            Owners         = $owner.UserPrincipalName;
        }
    }
}

if ($listAll -eq $False) {
    $results = $results | Where-Object {$_.ExpiryDate -le $expiryDate.AddDays(1) }
    "$($results.Count) passwords/keys expire by $($expiryDate.Date.ToShortDateString())"
}

if ($outputToCsvFile) {
    $now = Get-Date -format "ddMMyyyyHHmm"
    $csvFile = "list of azure ad applications - $($now).csv"
        "" | Set-Content $csvFile
    $results | Export-Csv $csvFile -NoTypeInformation
    Write-Host "Output saved to: $((Get-Item -Path ".\").FullName)\$($csvFile)" -BackgroundColor green -ForegroundColor Black
}
else {
    if($results.Count -gt 0) {
        "List of passwords/keys"
        $results | Format-Table -AutoSize
    }
}