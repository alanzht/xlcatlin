﻿param(
    [parameter(Mandatory=$true)][string]$subscription,
    [parameter(Mandatory=$false)][switch]$runAsJob = $false
)

$subscription_id = Get-AzureRmSubscription -SubscriptionName $subscription
# Change subscription
Select-AzureRmSubscription -SubscriptionID $subscription_id;
# Check subscription is correct before continuing 
$subscription_output=(Get-AzureRmContext).Subscription.Name;
if ( $subscription_output -ne $subscription ) {
    exit 1;
}
Else{
    Write-Host "Using $subscription_output Subsciption";
}

Write-Host "Deleting all resource groups in subscription name : " -NoNewline
Write-Host $subscription -ForegroundColor Yellow -NoNewline
Write-Host " with ID : " -NoNewline
Write-Host $subscription_id -ForegroundColor Yellow
$confirmation = Read-Host "Are you Sure You Want To Proceed (yes/no)"
if ($confirmation -eq 'yes') {
    $resourceGroups = Get-AzureRmResourceGroup
    foreach ($resourceGroup in $resourceGroups){
        try{
            Write-Host "Deleting resource group : " -NoNewline
            Write-Host "$($resourceGroup.ResourceGroupName)" -ForegroundColor Green
            if ($runAsJob){
                Remove-AzureRmResourceGroup -Name "$($resourceGroup.ResourceGroupName)" -Force -AsJob
            }else{
                Remove-AzureRmResourceGroup -Name "$($resourceGroup.ResourceGroupName)" -Force
            }
        }catch{
            $ErrorMessage = $_.Exception.Message
            Write-Error "Error : $ErrorMessage"
        }
    }
}else{
    Write-Host "canceling"
}