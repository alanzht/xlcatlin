param(
    [parameter(Mandatory=$true)][string[]]$resourceGroups
)

foreach ($resourceGroup in $resourceGroups){
    $vms = Get-AzureRmResource -ResourceType "Microsoft.Compute/virtualMachines" -ResourceGroupName $resourceGroup
    foreach ($vm in $vms){
        $vmName = $vm.Name
        $encryption_status = Get-AzureRmVmDiskEncryptionStatus -ResourceGroupName $resourceGroup `
                                -VMName $vmName -ErrorAction SilentlyContinue 
        if(-Not $encryption_status){
            $encryption_status = @{
                OsVolumeEncrypted = "NotEncrypted";
                DataVolumesEncrypted = "NotEncrypted";
            }
        }
        Write-Host "$(Get-Date) $vmName OsVolume : $($encryption_status.OsVolumeEncrypted)"
        Write-Host "$(Get-Date) $vmName DataVolumes : $($encryption_status.DataVolumesEncrypted)"
    }
}
