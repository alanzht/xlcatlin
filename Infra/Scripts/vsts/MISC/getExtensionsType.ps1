param(
    [string]$location
)

Get-AzureRmVmImagePublisher -Location $location | `
Get-AzureRmVMExtensionImageType | `
Get-AzureRmVMExtensionImage | Select-object Type, Version, PublisherName | Where-Object Type -eq "LinuxRHEL7"