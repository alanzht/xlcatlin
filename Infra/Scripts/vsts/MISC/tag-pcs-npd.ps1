﻿param (
    [string] $tagsFile = '..\..\parameters\tags\pcs.npd.json',
    [PSObject] $resourceGroupsToTag = @(
        [PSCustomObject]@{
            "rgName"      = "automation-tools"
            "vmRoleValue" = "app"
            "rgRoleValue" = "automation-tools"
        },
        [PSCustomObject]@{
            "rgName"      = "azlog"
            "vmRoleValue" = "app"
            "rgRoleValue" = "az-log"
        },
        [PSCustomObject]@{
            "rgName"      = "bastion"
            "vmRoleValue" = "btn"
            "rgRoleValue" = "bastion"
        },
        [PSCustomObject]@{
            "rgName"      = "deploy-aid"
            "vmRoleValue" = "app"
            "rgRoleValue" = "deploy-aid"
        },
        [PSCustomObject]@{
            "rgName"      = "domain-services"
            "vmRoleValue" = "ds"
            "rgRoleValue" = "domain-services"
        },
        [PSCustomObject]@{
            "rgName"      = "image-management"
            "vmRoleValue" = "unknown"
            "rgRoleValue" = "image-management "
        },
        [PSCustomObject]@{
            "rgName"      = "main-net"
            "vmRoleValue" = "unknown"
            "rgRoleValue" = "main-net"
        },
        [PSCustomObject]@{
            "rgName"      = "operations"
            "vmRoleValue" = "unknown"
            "rgRoleValue" = "operations"
        },
        [PSCustomObject]@{
            "rgName"      = "security"
            "vmRoleValue" = "unknown"
            "rgRoleValue" = "security"
        },
        [PSCustomObject]@{
            "rgName"      = "workspace"
            "vmRoleValue" = "unknown"
            "rgRoleValue" = "workspace"
        },
        [PSCustomObject]@{
            "rgName"      = "xlc-azu-eus2-npd-pcs-rg-deploy-aid"
            "vmRoleValue" = "app"
            "rgRoleValue" = "deploy-aid"
        }
    )
)


Get-Content $tagsFile


foreach ($envGroup in $resourceGroupsToTag) {
    [PSObject] $rg = Get-AzureRmResourceGroup -Name $($envGroup.rgName)
    [string] $tagsJson = Get-Content $tagsFile
    [string] $vmRoleValue = $($envGroup.vmRoleValue)
    [string] $rgRoleValue = $($envGroup.rgRoleValue)
    

    $tags = ($tagsJson | ConvertFrom-Json).Tags
    $tagsToAdd = @{}
    
    
    foreach ($tag in $tags)
    {
        [string] $value = $tag.Default

        switch ($tag.Name) {
            "Region" {
                $value = $rg.Location
                break
            }
            "ResourceType" {
                $value = "resourceGroup"
                break
            }
            "Role" {
                $value = $rgRoleValue
                break
            }
            Default {
                break
            }
        }

        $tagsToAdd.Add($tag.Name, $value)
    }


    #$tagsToAdd | Format-Table
    try{
        Set-AzureRmResourceGroup -Tag $tagsToAdd -Name $rg.ResourceGroupName -ErrorAction Stop
    }catch{
        $ErrorMessage = $_.Exception.Message
        Write-Warning "Error : $ErrorMessage"
    }
    foreach ($resource in $(Get-AzureRmResource -ResourceGroupName $rg.ResourceGroupName)) {
        try{
            $tagsToAdd["ResourceType"] = $resource.ResourceType.Substring($resource.ResourceType.LastIndexOf('/') + 1)
            $tagsToAdd["Region"] = $resource.Location
            # Resource group role is applied to all the respiurces except VM resources
            $tagsToAdd["Role"] = $rgRoleValue
            [bool] $vmResource = $($resource.ResourceType -eq 'Microsoft.Compute/virtualMachines')

            if ($vmResource) {
                $tagsToAdd["Role"] = $vmRoleValue
            }

            #$tagsToAdd | Format-Table
            Set-AzureRmResource -ResourceId $resource.ResourceId -Tag $tagsToAdd -ErrorAction Stop -Force
        }catch{
            $ErrorMessage = $_.Exception.Message
            Write-Warning "Error : $ErrorMessage"
        }
    }
}