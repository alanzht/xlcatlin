if ( Get-TypeData -TypeName System.Array) {
    Remove-TypeData System.Array 
    # Fix bug with arrays & json
    # https://stackoverflow.com/questions/20848507/why-does-powershell-give-different-result-in-one-liner-than-two-liner-when-conve/38212718#38212718
}

function Set-XLCAzureDeployResourceGroups {
    [CmdletBinding()]
    param(
        [parameter(Mandatory = $true)]
        [string]$AppSubscriptionName,        

        [parameter(Mandatory = $true)]
        [string]$location,        

        [parameter(Mandatory = $true)]
        [string]$ParameterFile,

        [parameter(Mandatory = $true)]
        [string]$TemplateFolder,

        [parameter(Mandatory = $true)]
        [object[]]$ResourceGroupList
    )

    Write-Verbose "Parameter Values"
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }

    if ( (Test-Path -Path "$ParameterFile") -eq $false ) {
        Write-Error "Error: Parameter File does not exist '$ParameterFile'"
        return
    }

    $CurrentFolder = (Get-Location).Path
    Write-Verbose "Current Folder: $CurrentFolder"
    
    Set-AzureRmContext -Subscription $AppSubscriptionName
    
    foreach ( $Group in $ResourceGroupList) {
        #$resourceGroupName = "$AppSubscriptionName-rg-$Group"
        #$templateFilePath = "$TemplateFolder\$Group\modules.azuredeploy.json"
        $resourceGroupName = $Group.Name
        $templateFilePath = "$TemplateFolder\$($Group.templatefolder)\modules.azuredeploy.json"

        $resourceGroup = Get-AzureRmResourceGroup -Name $resourceGroupName -ErrorAction SilentlyContinue
        if (!$resourceGroup) {
            Write-Host "Resource group '$resourceGroupName' does not exist."
            Write-Host "Creating resource group '$resourceGroupName' in location '$location'"
            New-AzureRmResourceGroup -Name $resourceGroupName -Location $location
        }
        else {
            Write-Host "Using existing resource group '$resourceGroupName'"
        }

        Write-Host "Starting deployment..."

        $Result = New-AzureRmResourceGroupDeployment `
            -ResourceGroupName $resourceGroupName `
            -TemplateFile $templateFilePath `
            -TemplateParameterFile $ParameterFile `
            -Verbose

        $Result | select ResourceGroupName, ProvisioningState, Timestamp, Mode

    }  
    Set-Location $CurrentFolder 
}

function Set-XLCAzurevNetPeering {    
    [CmdletBinding()]
    param(        
        [string]$ScriptFiles = "$((Get-Location).Path)",

        [parameter(Mandatory = $true)]
        [string]$vNETPeeringName,

        [parameter(Mandatory = $true)]
        [string]$FromSubscriptionName,

        [parameter(Mandatory = $true)]
        [string]$FromResourceGroupName,

        [parameter(Mandatory = $true)]
        [string]$ToSubscriptionName,

        [parameter(Mandatory = $true)]
        [string]$ToResourceGroupName
    )
    
    Write-Verbose "Parameter Values"
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }

    if ( (Test-Path -Path "$ScriptFiles") -eq $false ) {
        Write-Error "Error: Scripts folder does not exist"
        return
    }

    $CurrentFolder = (Get-Location).Path
    Write-Verbose "Current Folder: $CurrentFolder"    
   
    # REMOVE #
    #PCS
    Set-AzureRmContext -Subscription $FromSubscriptionName
    & "$ScriptFiles\Scripts\vsts\CD\removeVnetPeering.ps1" -groupName $FromResourceGroupName -name $vNETPeeringName
    $FromRemoteVnetId = (Get-AzureRmResource -ResourceGroupName $FromResourceGroupName -ResourceType Microsoft.Network/virtualNetworks).ResourceId
    #APP
    Set-AzureRmContext -Subscription $ToSubscriptionName
    & "$ScriptFiles\Scripts\vsts\CD\removeVnetPeering.ps1" -groupName $ToResourceGroupName -name $vNETPeeringName
    $ToRemoteVnetId = (Get-AzureRmResource -ResourceGroupName $ToResourceGroupName -ResourceType Microsoft.Network/virtualNetworks).ResourceId

    # ADD #
    #PCS
    Set-AzureRmContext -Subscription $FromSubscriptionName
    & "$ScriptFiles\Scripts\vsts\CD\vnetPeering.ps1" -groupName $FromResourceGroupName -remoteVnetId $ToRemoteVnetId -name $vNETPeeringName -AllowGatewayTransit
    #APP
    Set-AzureRmContext -Subscription $ToSubscriptionName
    & "$ScriptFiles\Scripts\vsts\CD\vnetPeering.ps1" -groupName $ToResourceGroupName -remoteVnetId $FromRemoteVnetId -name $vNETPeeringName -UseRemoteGateways

    Set-Location $CurrentFolder    

}

function Set-XLCAzurePCSRunbooks {
    [CmdletBinding()]
    param(
        [parameter(Mandatory = $true)]
        [string]$PCSSubscriptionName,

        [parameter(Mandatory = $true)]
        [string]$ResourceGroupName,

        [parameter(Mandatory = $true)]
        [string]$parameters,
        
        [parameter(Mandatory = $true)]
        [string]$TemplateFolder
    )

    Write-Verbose "Parameter Values"
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }
   
    if ( (Test-Path -Path "$parameters") -eq $false ) {
        Write-Error "Error: Parameter File does not exist '$parameters'"
        return
    }

    $CurrentFolder = (Get-Location).Path
    Write-Verbose "Current Folder: $CurrentFolder"
    
    Set-AzureRmContext -Subscription $PCSSubscriptionName

    $Result = New-AzureRmResourceGroupDeployment `
        -ResourceGroupName $ResourceGroupName `
        -TemplateFile "$TemplateFolder\pcs-operations-runbooks\modules.azuredeploy.json" `
        -TemplateParameterFile  $parameters `
        -Verbose

    $Result | select ResourceGroupName, ProvisioningState, Timestamp, Mode

    Set-Location $CurrentFolder    
}

function Set-XLCAzurePCSUploadArtifacts {
    [CmdletBinding()]
    param (             
        [parameter(Mandatory = $true)]
        [string]$location,    

        [parameter(Mandatory = $true)]
        $PCSSubscriptionName,

        [parameter(Mandatory = $true)]
        $PCSDeployAidRGName,

        [parameter(Mandatory = $true)]
        $PCSDeployAidStorageAccountName,

        [parameter(Mandatory = $true)]
        $UploadListJsonFile,

        [parameter(Mandatory = $false)]
        [string]$ModuleFolder = "$((Get-Location).Path)\Scripts\Modules"
    )

    Write-Verbose "Parameter Values"; foreach ($key in $PSBoundParameters.Keys) { Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key]) }

    if ( (Test-Path -Path "$ModuleFolder") -eq $false ) {
        Write-Error "Error: Scripts folder does not exist"
        return
    }

    if ( (Test-Path -Path "$UploadListJsonFile") -eq $false ) {
        Write-Error "Error: Upload List Json File does not exist: $UploadListJsonFile"
        return
    }

    $CurrentFolder = (Get-Location).Path
    Write-Verbose "Current Folder: $CurrentFolder"       
    $uploadList = (Get-Content -Raw "$UploadListJsonFile" | ConvertFrom-Json).uploadList

    Import-module -Name "$ModuleFolder\newDeployAid"       

    Set-AzureRmContext -Subscription $PCSSubscriptionName

    New-deployAid   -location $location `
        -groupName $PCSDeployAidRGName `
        -storageAccountName $PCSDeployAidStorageAccountName `
        -folderPaths $uploadList

    Remove-module -Name "newDeployAid"
    Set-Location $CurrentFolder
}

function New-XLCAzureParameterFile {
    [CmdletBinding()]
    param(
        [parameter(Mandatory = $true)]
        [string]$SubscriptionName,

        [parameter(Mandatory = $true)]
        [string]$Location,

        [parameter(Mandatory = $true)]
        [string]$SubscriptionType
    )
    Write-Verbose "Parameter Values: "
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }

    $ParamsFolder = ".\Environments\$SubscriptionType\Parameters\$SubscriptionName"
    $ParamsCommonFolder = ".\Environments\$SubscriptionType\Parameters\Common Parameter Templates"
    #$ParamFile          = ".\Environments\$SubscriptionType\Parameters\$SubscriptionName-parameters.json"
    $ParamFile = ".\$SubscriptionName-parameters.json"

    Write-Verbose "Creating SubScription details parameter file"
    .\scripts\vsts\CI\buildParamFile.ps1 `
        -ParameterFolder $ParamsFolder `
        -ParameterFolderCommon $ParamsCommonFolder `
        -OutputFile $ParamFile `
        -Verbose
}

function Get-XLCAzureDetails {
    [CmdletBinding()]
    param(
        [parameter(Mandatory = $true)]
        [string]$SubscriptionAlias
    )
    Write-Verbose "Parameter Values: "
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }

    $data = Get-Content -Path "$PSScriptRoot\Environments.json" | convertfrom-json

    $Tenant = $data.Tenents | ? {$_.regions.environments.subscriptions.alias -eq $SubscriptionAlias }
    $Region = $Tenant.regions | ? {$_.environments.subscriptions.alias -eq $SubscriptionAlias }
    $Environment = $Region.environments | ? {$_.subscriptions.alias -eq $SubscriptionAlias }
    $Subscription = $Environment.subscriptions | ? {$_.alias -eq $SubscriptionAlias }

    $PCSSubscription = $Environment.subscriptions | ? {$_.type -eq "pcs" }    
    if ( $PCSSubscription -eq $null -or $PCSSubscription.count -gt 1 )
    {
        Write-Error "Didnt find PCS subscription for region"
        return $null
    }

    $Details = [ordered]@{
        "Region"           = $Region.name
        "Environment"      = $Environment.name
        
        "VMNameId"              = $Subscription.vmid
        "AdminUser"             = "XLSupport"
        "AdminPassword"         = "C0ntr0170"        

        "Chef"                  = $Environment.chef
             
        "Tenant" = [ordered]@{
                        "Id"              = $Tenant.id
                        "Name"            = $Tenant.name
        }

        "Subscription" = [ordered]@{                                     
                            "Id"                = $Subscription.id
                            "Name"              = $Subscription.name                            
                            "Type"              = $Subscription.type
                            "DiagStorageAcc"    = $Subscription.DiagStorageAcc
                            "vNetPeeringName"   = $Subscription.vNetPeeringName
                            "KeyVault"          = $Subscription.KeyVault
                            "ResourceGroups" = [ordered]@{
                                "MainNet"    = $Subscription.RGMainNet
                                "Operations" = $Subscription.RGOperations                                
                            }
                            "ResourceGroupList" = $Subscription.ResourceGroupList
                            
        }

        "PCSSubscription" = [ordered]@{ 
                            "Id"        = $PCSSubscription.id
                            "Name"      = $PCSSubscription.name                            
                            "ResourceGroups" = [ordered]@{
                                "MainNet"    = $PCSSubscription.RGMainNet
                                "Operations" = $PCSSubscription.RGOperations                                
                            }
        }

         "artifactrepository" = [ordered]@{ 
                                "uri"= $Environment.artifactrepository.uri
                                "secret"= $Environment.artifactrepository.secret
                                "resourcegroupname" = $Environment.artifactrepository.resourcegroupname
                                "storageaccount" = $Environment.artifactrepository.storageaccount
                                "subscriptionname" = $Environment.artifactrepository.subscriptionname
                                "UploadListJsonFile" = $Environment.artifactrepository.UploadListJsonFile
                            }
        
    }

    return $Details
}

function Set-XLCAzureTags {
    [CmdletBinding()]
    param(       
        [parameter(Mandatory = $true)]
        [string]$AppFolder,

        [parameter(Mandatory = $true)]
        [string]$SubscriptionName,      

        [parameter(Mandatory = $false)]
        [string]$ModuleFolder = "$((Get-Location).Path)\Scripts\Modules"
    )

    Write-Verbose "Parameter Values"
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }

    if ( (Test-Path -Path $ModuleFolder) -eq $false ) {
        Write-Error "Error: Powershell Module folder does not exist"
        return
    }

    if ( (Test-Path -Path $AppFolder) -eq $false ) {
            Write-Error "Error: AppFolder folder does not exist: $Folder"
            return
        }

    if ( (Test-Path -Path "$AppFolder\Settings.json") -eq $false ) {
        Write-Error "Error: AppFolder does not contain settings.json: $Folder"
        return
    }

    $CurrentFolder = (Get-Location).Path
    Write-Verbose "Current Folder: $CurrentFolder"

    $tagsJson = Get-Content -path "$AppFolder\Settings.json" | ConvertFrom-Json | select @{n="tags";e={  $_.Tags }}  | ConvertTo-Json -Depth 100
    
    Set-AzureRmContext -Subscription $SubscriptionName

    Import-module -Name "$ModuleFolder\addTags"
    Add-TagsToAllGroupsAndResources -tagsJson $tagsJson.tostring()
    Remove-module -Name addTags      
    Set-Location $CurrentFolder 
}

function Set-XLCAzurePolicies {
    [CmdletBinding()]
    param(        
        [string]$ScriptFiles = "$((Get-Location).Path)",

        [parameter(Mandatory = $true)]
        [string]$SubscriptionName
    )

    Write-Verbose "Parameter Values"
    foreach ($key in $PSBoundParameters.Keys) {
        Write-Verbose ("  $($key)" + ' = ' + $PSBoundParameters[$key])
    }

    if ( (Test-Path -Path "$ScriptFiles") -eq $false ) {
        Write-Error "Error: Scripts folder does not exist"
        return
    }

    $CurrentFolder = (Get-Location).Path
    Write-Verbose "Current Folder: $CurrentFolder"
    
    Set-AzureRmContext $SubscriptionName

    & "$ScriptFiles\Scripts\vsts\CD\createPolicies.ps1"

    Set-Location $CurrentFolder
    
}

function Get-XLCVMDetails {
    begin {
        $subscription = (Get-AzureRmContext).Subscription.Name
        #$Nics = Get-AzureRmNetworkInterface
    }
    process {

        $Disks = @()
        $Extensions = @()

        foreach ($disk in $_.StorageProfile.DataDisks ) {
            $Disks += $disk.DiskSizeGB
        }

     #   foreach ($extension in $_.Extensions ) {
     #       $Extensions += "$($extension.VirtualMachineExtensionType) ($($extension.TypeHandlerVersion))"
     #   }

        $Encryption = Get-AzureRmVmDiskEncryptionStatus -VMName $_.Name -ResourceGroupName $_.ResourceGroupName

        $pro = [ordered]@{
            Subscription         = $subscription
            Name                 = $_.Name
            ComputerName         = $_.OSProfile.ComputerName
            ResourceGroupName    = $_.ResourceGroupName
            Location             = $_.Location
            VMSize               = $_.HardwareProfile.VmSize
            # ProvisioningState       = $_.ProvisioningState
            OSType               = $_.StorageProfile.OsDisk.OsType
            OSSku                = $_.StorageProfile.ImageReference.Sku
            OSDiskSizeGB         = $_.StorageProfile.OsDisk.DiskSizeGB
            DataDisks            = $Disks
            #Extensions           = $Extensions
            EncryptedProgressMessage = $Encryption.ProgressMessage
            OsVolumeEncrypted    = $Encryption.OsVolumeEncrypted
            DataVolumesEncrypted = $Encryption.DataVolumesEncrypted
            #PrivateIP            = ""
            #PublicIP             = ""
            #SubNet               = ""

        }

        $obj = New-Object -Property $pro -TypeName psobject
        $obj
    }
}

<#

$SubDetails = Get-XLCAzureDetails -SubscriptionAlias EUS2-NPD-INFRA
Set-AzureRmContext $details.Subscription.Id
$vmlist = Get-AzureRmVM | Get-XLCVMDetails

$vmlist[3] | %{ `
    .\extensionEncryption.ps1 `
        -VM $_.Name `
        -resourceGroup $_.ResourceGroupName `
        -keyVaultName $SubDetails.Subscription.KeyVault `
        -artifactUri $SubDetails.artifactrepository.uri `
        -artifactSecret $SubDetails.artifactrepository.secret `
        -verbose
}

#>