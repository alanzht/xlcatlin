<#
    .SYNOPSIS
        Runbook to Change Resource Lock state between ReadOnly and CanNotDelete.

    .DESCRIPTION
        Provides a utility to change a lock's status between either ReadOnly or CanNotDelete.

        When a resource is in the ReadOnly mode, no one can perform any modify operations.
        
        When a resource is in the CanNotDelete state, no one can perform any delete operations, 
        but they can commit changes to the resource.

    .PARAMETER ResourceGroupName 
        Mandatory 
        Specify the Resource Group which contains the resource to perform the lock operation

    .PARAMETER Resource
        Mandatory 
        Specify the Resource to perform the lock operation. 

    .PARAMETER LockLevel
        Mandatory
        Specify the LockLevel - values can either be [ReadOnly] or [CanNotDelete].         

    .NOTES
        AUTHOR: Alan Tan
        CREATED: 15 October, 2018
        LASTEDIT: 16 October, 2018

    .EXAMPLE
        Set-Lock-Mode -ResourceGroupName <rg> -ResourceName <resource> -LockLevel "CanNotDelete"
        Set-Lock-Mode -ResourceGroupName <rg> -ResourceName <resource> -LockLevel "ReadOnly"
#>
Function Set-LockMode {
    param
    (
      [Parameter (Mandatory= $true)]
      [String]$ResourceName,
      [Parameter (Mandatory= $true)]
      [ValidateSet("CanNotDelete", "ReadOnly")]
      [String]$ResourceLockLevel = "ReadOnly"
    )    

    $LockNotes = "Resource Lock on: $ResourceName."
    try {
        $resource = Get-AzureRMResource -Name $ResourceName
        If(!$resource) {
            Write-Output "Error finding resource: $resourceName"
        }
        Else {
            $resourceGroupName = $resource.ResourceGroupName
            $resourceType = $resource.Type
            $resourceLock = Get-AzureRMResourceLock -ResourceGroupName $ResourceGroupName -ResourceName $ResourceName -ResourceType $resourceType -ErrorAction SilentlyContinue
            If($resourceLock) {
                $LockName     = $resourceLock.Name
                $ResourceType = $resourceLock.ResourceType
                $LockLevel    = $resourceLock.Properties.Level
                $LockNotes    = $resourceLock.Properties.Notes

                If($LockLevel -eq $ResourceLockLevel) {
                    Write-Output "Resource Lock Level on $resourceName is already set to: $resourceLockLevel. Nothing to do."
                }
                Else {
                    If($LockNotes) {
                        $result = Set-AzureRMResourceLock -LockId $resourceLock.LockId -LockLevel $ResourceLockLevel -LockNotes $LockNotes -Force
                    }
                    Else {
                        $result = Set-AzureRMResourceLock -LockId $resourceLock.LockId -LockLevel $ResourceLockLevel -Force
                    }
                    If($result.Properties.level -eq $ResourceLockLevel) {
                        Write-Output $result
                    }
                    Else {
                        $ErrorMessage = "An error was encountered when changing the Lock Status to $ResourceLockLevel on Resource Lock: $LockName on the Resource: $ResourceName."
                        Write-Error $ErrorMessage
                    }
                }
            }
            Else {
                $resourceLockName = $ResourceName + "-Lock"
                try {
                    $result = New-AzureRMResourceLock -LockName $resourceLockName -LockLevel $ResourceLockLevel -LockNotes $LockNotes -ResourceName $ResourceName -ResourceGroupName $resourceGroupName -ResourceType $resourceType -Force
                    Write-Output "Creating New Resource Lock: $resourceLockName on resource: $resourceName with Lock Level - ""$resourceLockLevel"""
                }
                Catch {
                    Write-Error -Message $_.Exception
                    throw $_.Exception               
                }

                If($result.Name -eq $resourceLockName) {
                    Write-Output $result
                }
                Else {
                    $ErrorMessage = "An error was encountered when creating new Resource Lock: $resourceLockName on resource: $ResourceName. Please check and try again."
                    Write-Error $ErrorMessage
                }
            }
        }
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception               
    }
}