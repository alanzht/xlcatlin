﻿<#
    .SYNOPSIS
        Dynamically Generates a Route Table and associates with applicable subnets.

    .DESCRIPTION
        Executed from within an Azure Service Principal that has access to the different 
        subscriptions (Contributor) contained within the Configuration File. The config file
        is written in JSON format, and contains all the associated routes.

    .PARAMETER ResourceGroupName 
        Mandatory 
        Specify the ResourceGroupName containing the Virtual Network and Route Table

    .PARAMETER virtualNetworkName 
        Mandatory 
        Specify the name of the Virtual Network containing the Subnets of which the Routing Tables are associated to.
        
    .PARAMETER routeTableName
        Mandatory 
        Specify the name of the Route Table which we want to modify and apply to the subnets within the Virtual Network

    .NOTES
        AUTHOR: Alan Tan
        CREATED: 17 October, 2018
        LAST UPDATED: 18 October, 2018
#>
# Import Set-LockMode Function
. ".\Set-LockMode.ps1"

Param (
    [Parameter (Mandatory = $true)]
    [string]$ResourceGroupName,
    [Parameter (Mandatory = $true)]
    [string]$virtualNetworkName,
    [Parameter (Mandatory = $true)]
    [string]$routeTableName
)
### Authenticate with Azure Automation account
$connectionName = "AzureRunAsConnection"
$WarningPreference = "SilentlyContinue"
$ErrorActionPreference = "Stop"

try {
    # Get the connection "AzureRunAsConnection "
    $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName

    "Logging in to Azure..."
    Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
}
catch {
    if (!$servicePrincipalConnection) {
        $ErrorMessage = "Connection $connectionName not found."
        throw $ErrorMessage
    }
    else {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}

### Check and Install the Azure Public IP Addresses Powershell Module
$AzurePublicIPAddressesModule = Get-Module -ListAvailable | Where {$_.Name -eq "AzurePublicIPAddresses"} | Out-Null
If(!$AzurePublicIPAddressesModule) {
    try {
        Install-Module -Name AzurePublicIPAddresses -Force
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}

### Path to Route Table configuration file
$configFile = ".\RouteTables-Master-Config.json"
### Variable for total Route count
$totalIPCount = 0
$config = Get-Content -Path $configFile -Raw | ConvertFrom-json

### Iterate through Configuration
ForEach($item in $config) {
    $subscription = $item.subscriptionName
    $result = Select-AzureRMSubscription -SubscriptionName $subscription
    If($result) {
        $resourceGroupName = $item.resourceGroupName
        $virtualNetworkName = $item.virtualNetworkName
        $resourceLocations = $item.resourceLocations

        $vNet = Get-AzureRmVirtualNetwork -ResourceGroupName $resourceGroupName -Name $virtualNetworkName
        $subnets = $vNet.Subnets | Where-Object {$_.Name -ne 'GatewaySubnet'} | Select-Object Name
        $routeTableName = $item.routeTableName

        ### Attempt to retrieve the route table
        $routeTable = Get-AzureRmRouteTable -Name $routeTableName -ResourceGroupName $resourceGroupName
        If(!$routeTable) {            
            ### If the route table doesn't exist, then create a new one in the provided resource group and location
            $routeTable = New-AzureRmRouteTable -Name $routeTableName -ResourceGroupName $resourceGroupName -Location $resourceLocation    
        }
        Else {
            # Ensure the Lock Mode is set to CanNotDelete
            Set-LockMode -ResourceName $routeTableName -ResourceLockLevel "CanNotDelete"

            $routeConfigs = Get-AzureRmRouteConfig -RouteTable $routeTable
            ### If the route table is found, then we clear all existing route configurations
            ForEach ($config in $routeConfigs) {
                Remove-AzureRmRouteConfig -RouteTable $routeTable -Name $config.Name | Out-Null
            }           
            $routeTable | Set-AzureRmRouteTable 
            If($routeTable.routes.count -ne 0) {
                Throw "Error removing existing routes. Please check and try again"
            }
            Else {
                Write-Output "Committed removal of routes in Route Table: $($routetable.name)."
            }
        }

        ### For each Microsoft Azure Datacenter Region, we obtain the IP ranges dynamically from the cmdlet
        ForEach($location in $resourceLocations) {
            Write-Output "Extracting Microsoft Azure Datacenter IP Ranges for Azure Region: $location"
            $MicrosoftIPRanges += Get-MicrosoftAzureDatacenterIPRange -AzureRegion $location
        }
        Write-Output "*Count* - Microsoft Azure Datacenter IP Count: $($MicrosoftIPRanges.Count)"
        $totalIPCount += $MicrosoftIPRanges.Count
        ### Add Microsoft Azure Datacenter Ranges to the Route Table
        ForEach($msRoute in $MicrosoftIPRanges) {
            $routeName = ($msRoute.Region.Replace(' ', '-').ToLower() + '-' + $msRoute.Subnet.Replace('/', '-'))
            $AddressPrefix = $msRoute.subnet
            $NextHopType   = "Internet"
            $routeTable | Add-AzureRmRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
        }

        ### Add Corporate Defined Authentication and Identity IP Ranges to the Route Table
        $authAndIdentityIps = @($item.authAndIdentityIps)
        Write-Output "*Count* - Auth and Identity IPs: $($authAndIdentityIps.Count)"
        $totalIPCount += $authAndIdentityIps.Count
        For($index = 0; $index -lt $authAndIdentityIps.Count; $index++) {
            $routeName = "auth-and-identity-ip$index"
            $AddressPrefix = $authAndIdentityIps[$index]
            $NextHopType = "Internet"
            $routeTable | Add-AzureRMRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
        }

        ### Add RHEL and YUM IP Ranges to the Route Table
        $RHELYumIps = @($item.RHELYumServers)
        Write-Output "*Count* - RHEL and YUM Server IPs: $($RHELYumIps.Count)"
        $totalIPCount += $RHELYumIps.Count
        For($index = 0; $index -lt $RHELYumIps.Count; $index++) {
            $routeName = "rhel-azureyum-ip$index"
            $AddressPrefix = $RHELYumIps[$index]
            $NextHopType = "Internet"
            $routeTable | Add-AzureRMRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
        }        

        ### Add Additional Custom Route IP Ranges to the Route Table
        $customRoutes = $item.customRoutes
        Write-Output "*Count* - Custom Route IPs: $($customRoutes.Count)"
        $totalIPCount += $customRoutes.Count
        ForEach($customRoute in $customRoutes) {
            $routeName = $customRoute.Name
            $AddressPrefix = $customRoute.AddressPrefix 
            $NextHopType = $customRoute.NextHopType
            $routeTable | Add-AzureRmRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
        }

        ### Provide a total IP range count of all the routes
        Write-Output "*Count* - Total IP Count: $totalIPCount"
        ### Commit the Route Table updates
        $routeTable | Set-AzureRMRouteTable
        ### Retrieve all the subnets that are within the Virtual Network
        $subnets = (Get-AzureRMVirtualNetwork -Name $virtualNetworkName -ResourceGroupName $resourceGroupName).subnets
        ForEach($subnet in $subnets) {
            ### Check that the subnet is listed in Applicable Subnets, otherwise skip applying the route table
            if($item.applicableSubnets -Contains $subnet.Name) {
                Write-Output "Applying Route Table Update to Subnet: $($subnet.Name)"
                $virtualNetwork = Get-AzureRMVirtualNetwork -ResourceGroupName $resourceGroupName -ResourceName $virtualNetworkName
                $UpdateSubnetConfig = $virtualNetwork | Set-AzureRmVirtualNetworkSubnetConfig -Name $subnet.Name -AddressPrefix $subnet.AddressPrefix -RouteTable $routeTable
                $virtualNetwork | Set-AzureRmVirtualNetwork -Verbose
            }
            else {
                Write-Output "Subnet $($subnet.Name) has been excluded from Route Table update."
            }
        }
        # Ensure that after changes are made, the Route Table has a Lock Mode set to ReadOnly.
        Set-LockMode -ResourceName $routeTableName -ResourceLockLevel "ReadOnly"
    }
    else {
        throw "Cannot Select the Subscription: $Subscription. Please check and try again."
    }
}