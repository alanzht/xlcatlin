### Authenticate with Azure Automation account
<#
$cred = "AzureRunAsConnection"
try {
    # Get the connection "AzureRunAsConnection "
    $servicePrincipalConnection = Get-AutomationConnection -Name $cred

    "Logging in to Azure..."
    Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
}
catch {
    if (!$servicePrincipalConnection) {
        $ErrorMessage = "Connection $cred not found."
        throw $ErrorMessage
    }
    else {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}
#>
. ".\Set-LockMode.ps1"
$AzurePublicIPAddressesModule = Get-Module -ListAvailable | Where {$_.Name -eq "AzurePublicIPAddresses"} | Out-Null
If(!$AzurePublicIPAddressesModule) {
    try {
        Install-Module -Name AzurePublicIPAddresses -Force
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}
$configFile = ".\RouteTables-Master-Config-Copy.json"
$totalIPCount = 0
$config = Get-Content -Path $configFile -Raw | ConvertFrom-json


### Iterate through Configuration
ForEach($item in $config) {
    $subscription = $item.subscriptionName
Write-Host "SubscriptionName: $subscription"    
    $result = Select-AzureRMSubscription -SubscriptionName $subscription
    If($result) {
        $resourceGroupName = $item.resourceGroupName
        $virtualNetworkName = $item.virtualNetworkName
        $resourceLocations = $item.resourceLocations
        
        $vNet = Get-AzureRmVirtualNetwork -ResourceGroupName $resourceGroupName -Name $virtualNetworkName -WarningAction SilentlyContinue
        $subnets = $vNet.Subnets | Where-Object {$_.Name -ne 'GatewaySubnet'} | Select-Object Name
        $routeTableName = $item.routeTableName

Write-Host "rg: $resourceGroupName"
Write-Host "vnet: $virtualNetworkName"
Write-Host "locations: $resourceLocations"
Write-Host "routeTable: $routeTableName"        

        ### Attempt to retrieve the route table
        $routeTable = Get-AzureRmRouteTable -Name $routeTableName -ResourceGroupName $resourceGroupName
        If(!$routeTable) {            
            ### If the route table doesn't exist, then create a new one in the provided resource group and location
            $routeTable = New-AzureRmRouteTable -Name $routeTableName -ResourceGroupName $resourceGroupName -Location $resourceLocation    
        }
        Else {
            # Ensure the Lock Mode is set to CanNotDelete
Write-Host "Setting Lock Mode Status!"
Sleep -Seconds 10        
            Set-LockMode -ResourceName $routeTableName -ResourceLockLevel "CanNotDelete"
Write-Host "Lock Mode Status Set to CanNotDelete"            
            $routeConfigs = Get-AzureRmRouteConfig -RouteTable $routeTable
            ### If the route table is found, then we clear all existing route configurations
            ForEach ($config in $routeConfigs) {
                Remove-AzureRmRouteConfig -RouteTable $routeTable -Name $config.Name | Out-Null
            }           
            $routeTable | Set-AzureRmRouteTable 
            If($routeTable.routes.count -ne 0) {
Write-Host "Error in removing route configs"
                Throw "Error removing existing routes. Please check and try again"
            }
            Else {
Write-Host "Committed removal of Route Table Route configuration..."
                Write-Output "Committed removal of routes in Route Table: $($routetable.name)."
            }
        }

        ### For each Microsoft Azure Datacenter Region, we obtain the IP ranges dynamically from the cmdlet
        ForEach($location in $resourceLocations) {
            Write-Output "Extracting Microsoft Azure Datacenter IP Ranges for Azure Region: $location"
            $MicrosoftIPRanges += Get-MicrosoftAzureDatacenterIPRange -AzureRegion $location
        }
        Write-Output "*Count* - Microsoft Azure Datacenter IP Count: $($MicrosoftIPRanges.Count)"
        $totalIPCount += $MicrosoftIPRanges.Count
        ### Add Microsoft Azure Datacenter Ranges to the Route Table
        ForEach($msRoute in $MicrosoftIPRanges) {
            $routeName = ($msRoute.Region.Replace(' ', '-').ToLower() + '-' + $msRoute.Subnet.Replace('/', '-'))
            $AddressPrefix = $msRoute.subnet
            $NextHopType   = "Internet"
            $routeTable | Add-AzureRmRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
Write-Host "$routeName | $AddressPrefix | $NextHopType"
        }

        ### Add Corporate Defined Authentication and Identity IP Ranges to the Route Table
        $authAndIdentityIps = @($item.authAndIdentityIps)
        Write-Output "*Count* - Auth and Identity IPs: $($authAndIdentityIps.Count)"
        $totalIPCount += $authAndIdentityIps.Count
        For($index = 0; $index -lt $authAndIdentityIps.Count; $index++) {
            $routeName = "auth-and-identity-ip$index"
            $AddressPrefix = $authAndIdentityIps[$index]
            $NextHopType = "Internet"
            $routeTable | Add-AzureRMRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
Write-Host "$routeName | $AddressPrefix | $NextHopType"
        }

        ### Add RHEL and YUM IP Ranges to the Route Table
        $RHELYumIps = @($item.RHELYumServers)
        Write-Output "*Count* - RHEL and YUM Server IPs: $($RHELYumIps.Count)"
        $totalIPCount += $RHELYumIps.Count
        For($index = 0; $index -lt $RHELYumIps.Count; $index++) {
            $routeName = "rhel-azureyum-ip$index"
            $AddressPrefix = $RHELYumIps[$index]
            $NextHopType = "Internet"
            $routeTable | Add-AzureRMRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
Write-Host "$routeName | $AddressPrefix | $NextHopType"
        }        

        ### Add Additional Custom Route IP Ranges to the Route Table
        $customRoutes = $item.customRoutes
        Write-Output "*Count* - Custom Route IPs: $($customRoutes.Count)"
        $totalIPCount += $customRoutes.Count
        ForEach($customRoute in $customRoutes) {
            $routeName = $customRoute.Name
            $AddressPrefix = $customRoute.AddressPrefix 
            $NextHopType = $customRoute.NextHopType
            $routeTable | Add-AzureRmRouteConfig -Name $routeName -AddressPrefix $AddressPrefix -NextHopType $NextHopType
Write-Host "$routeName | $AddressPrefix | $NextHopType"
        }

        ### Provide a total IP range count of all the routes
        Write-Output "*Count* - Total IP Count: $totalIPCount"
Write-Host "TOTAL IP COUNT: $totalIPCount"        
        ### Commit the Route Table updates
        $routeTable | Set-AzureRMRouteTable
        ### Retrieve all the subnets that are within the Virtual Network
        $subnets = (Get-AzureRMVirtualNetwork -Name $virtualNetworkName -ResourceGroupName $resourceGroupName -WarningAction SilentlyContinue).subnets
        ForEach($subnet in $subnets) {
            ### Check that the subnet is listed in Applicable Subnets, otherwise skip applying the route table
            if($item.applicableSubnets -Contains $subnet.Name) {
                Write-Output "Applying Route Table Update to Subnet: $($subnet.Name)"
                $virtualNetwork = Get-AzureRMVirtualNetwork -ResourceGroupName $resourceGroupName -ResourceName $virtualNetworkName -WarningAction SilentlyContinue
                $UpdateSubnetConfig = $virtualNetwork | Set-AzureRmVirtualNetworkSubnetConfig -Name $subnet.Name -AddressPrefix $subnet.AddressPrefix -RouteTable $routeTable -WarningAction SilentlyContinue
                $virtualNetwork | Set-AzureRmVirtualNetwork -Verbose -WarningAction SilentlyContinue
            }
            else {
                Write-Output "Subnet $($subnet.Name) has been excluded from Route Table update."
            }
        }
Write-Host "Setting the Lock MODE on routeTable: $routeTableName - back / or for the first time to ReadOnly"
        Set-LockMode -ResourceName $routeTableName -ResourceLockLevel "ReadOnly"
    }
    else {
        throw "Cannot Select the Subscription: $Subscription. Please check and try again."
    }
}