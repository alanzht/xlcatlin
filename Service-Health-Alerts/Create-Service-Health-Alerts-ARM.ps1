<#
    .SYNOPSIS
        Iterates through subscriptions and sets the Service Health Alerts to
        the Action Group and Defined Action E-mail

    .DESCRIPTION
        Service Health Alerts are configured per subscription for different 
        Resource Groups based on the XLC requirements

    .NOTES
        AUTHOR: Alan Tan
        CREATED: 17 October, 2018
#>

$ConfigurationFile = ".\SHA-Master-Config.json"
$DeploymentTemplate = ".\ServiceHealthAlerts.json"
$Config = Get-Content -Path $ConfigurationFile -Raw | ConvertFrom-Json

ForEach($SHAItem in $Config) {
    $SubscriptionName     = $SHAItem.subscriptionName
    $ResourceGroupName    = $SHAItem.resourceGroup
    $DeploymentParameters = ".\" + $SHAItem.subscriptionName + "-SHA-Params.json"
    try {
        $Subscription = Select-AzureRmSubscription -SubscriptionName $SubscriptionName
    }
    catch {
        If(!$Subscription) {
            throw "Error connecting to Subscription: $SubscriptionName. Please check configuration file and try again."
        }
        Else {
            Write-Error -Message $_.Exception
            throw $_.Exception        
        }
        Continue
    }
    New-AzureRmResourceGroupDeployment -Name "Deploy-SHA-$SubscriptionName" -ResourceGroupName $ResourceGroupName -TemplateFile $DeploymentTemplate -TemplateParameterFile $DeploymentParameters
}
