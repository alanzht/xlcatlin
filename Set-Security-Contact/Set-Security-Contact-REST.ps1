<#
    .SYNOPSIS
        Sets the Security Contact Details in Azure Security Center

    .DESCRIPTION
        Azure Security Center will recommend that you provide security contact details 
        for your Azure subscription if you haven’t already. This information will be 
        used by Microsoft to contact you if the Microsoft Security Response Center (MSRC)
        discovers that your customer data has been accessed by an unlawful or unauthorized 
        party. MSRC performs select security monitoring of the Azure network and infrastructure 
        and receives threat intelligence and abuse complaints from third parties.

        An email notification is sent on the first daily occurrence of an alert and only for high 
        severity alerts. Email preferences can only be configured for subscription policies. 
        Resource groups within a subscription will inherit these settings.
        
        Assumption that PowerShellGet is installed on the machine.

    .NOTES
        AUTHOR: Alan Tan
        CREATED: 16 October, 2018
#>

#Requires -Module AzureRM.Security
$cred = "AzureRunAsConnection"
try {
    # Get the connection "AzureRunAsConnection "
    $servicePrincipalConnection = Get-AutomationConnection -Name $cred

    "Logging in to Azure..."
    Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
}
catch {
    if (!$servicePrincipalConnection) {
        $ErrorMessage = "Connection $cred not found."
        throw $ErrorMessage
    }
    else {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}

# Login uri for Azure AD
$LoginURI         = "https://login.windows.net/"
$ResourceAppIdURI = "https://management.core.windows.net/"

#$ConfigFile = ".\Security-Contact-Config.json"
#$SecurityConfig = Get-Content -Path $ConfigFile -Raw | ConvertFrom-JSON

$SecurityConfig = @()

$securityItem = @{
    subscriptionName = "Visual Studio Enterprise";
    email = "alan.tan@hpe.com,alan.tan@contractor.xlcatlin.com,alanzhtan@gmail.com";
    alertNotifications = "On";
    alertAdmins = "Off"
}
$securityItem2 = @{
    subscriptionName = "Hello";
    email = "hello@hello.com";
    alertNotifications = "On";
    alertAdmins = "Off"      
}
$SecurityConfig += $SecurityItem
$SecurityConfig += $SecurityItem2

ForEach($SecurityContact in $SecurityConfig) {
    $SubscriptionName = $SecurityContact.SubscriptionName
    Write-Output "Setting the Security Contact for Subscription: ""$SubscriptionName"""
    try {
        $Subscription = Get-AzureRmSubscription -SubscriptionName $SubscriptionName
        If($Subscription) {
            Select-AzureRmSubscription -SubscriptionObject $Subscription
            $subscriptionID = $Subscription.Id

            # Get AutomationCertificate
            $Certifcate = Get-AutomationCertificate -Name "AzureRunAsCertificate"
            # Set up authentication using service principal client certificate
            $Authority = $LoginURI + $servicePrincipalConnection.TenantId
            $AuthContext = New-Object "Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext" -ArgumentList $Authority
            $ClientCertificate = New-Object "Microsoft.IdentityModel.Clients.ActiveDirectory.ClientAssertionCertificate" -ArgumentList $servicePrincipalConnection.ApplicationId, $Certifcate
            $AuthResult = $AuthContext.AcquireToken($ResourceAppIdURI, $ClientCertificate)

            # Set up header with authorization token
            $AuthToken = $AuthResult.CreateAuthorizationHeader()
            $RequestHeader = @{
                "Content-Type" = "application/json";
                "Authorization" = "$AuthToken"
            }           
       }
       Else {
           throw "The Subscription: $SubscriptionName is not found. Please check and try again."
       }
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception
        exit
    }

    # Retrieve the number of Security Contacts in the Subscription
    $numContacts = (Get-AzureRmSecurityContact).count
    For($index = $numcontacts; $index -gt 0; $index--) {
        $securityContactName = "default" + $index
        $RESTURI = "https://management.azure.com/subscriptions/" + $subscriptionId + `
        "/providers/Microsoft.Security/securityContacts/" + $securityContactName + `
        "?api-version=2017-08-01-preview"

        $Result = Invoke-RestMethod -Method 'DELETE' -Uri $RESTURI -Header $requestHeader
        Write-Output "REST API Call Returned: $Result"
    }

    $Emails = $SecurityContact.Email.Split(",")
    For($index = 1; $index -le $Emails.Count; $index++) {        
        $securityContactName = "default" + $index
        $body = @{
            name = $securityContactName
            type = "Microsoft.Security/securityContacts"
        }
        $properties = @{}        
        # Only the first index can have a phone number, this field is optional
        If(($index -eq 1) -and ($SecurityContact.Phone -ne $Null))  {
            $properties.Add("phone",$SecurityContact.Phone)
        }
        $properties.Add("email", $Emails[$index])
        $properties.Add("alertNotifications", "On")
        $properties.Add("alertToAdmins", "Off")
        $body.Add("properties",$properties)

        $RESTURI = "https://management.azure.com/subscriptions/" + $subscriptionId + `
            "/providers/Microsoft.Security/securityContacts/" + $securityContactName + `
            "?api-version=2017-08-01-preview"
            
        $json = $body | ConvertTo-Json
        $Result = Invoke-RestMethod -Method 'PUT' -Uri $RESTURI -Header $requestHeader -Body $json -ContentType 'application/json'
        Write-Output "REST API Call Returned: $Result"
    }
}