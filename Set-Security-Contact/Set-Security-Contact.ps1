<#
    .SYNOPSIS
        Sets the Security Contact Details in Azure Security Center

    .DESCRIPTION
        Azure Security Center will recommend that you provide security contact details 
        for your Azure subscription if you haven’t already. This information will be 
        used by Microsoft to contact you if the Microsoft Security Response Center (MSRC)
        discovers that your customer data has been accessed by an unlawful or unauthorized 
        party. MSRC performs select security monitoring of the Azure network and infrastructure 
        and receives threat intelligence and abuse complaints from third parties.

        An email notification is sent on the first daily occurrence of an alert and only for high 
        severity alerts. Email preferences can only be configured for subscription policies. 
        Resource groups within a subscription will inherit these settings.
        
        Assumption that PowerShellGet is installed on the machine.

    .NOTES
        AUTHOR: Alan Tan
        CREATED: 16 October, 2018
#>

$AzureRMSecurityModule = Get-Module -ListAvailable | Where {$_.Name -eq "AzureRM.Security"}
If(!$AzureRMSecurityModule) {
    try {
        Install-Module -Name AzureRM.Security -AllowPrerelease -Force
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}
$ConfigFile = ".\Security-Contact-Config.json"
$SecurityConfig = Get-Content -Path $ConfigFile -Raw | ConvertFrom-JSON
ForEach($SecurityContact in $SecurityConfig) {
    $Subscription = $SecurityContact.SubscriptionName
    try {
       Select-AzureRmSubscription -SubscriptionName $Subscription
    }
    catch {
        Write-Error -Message $_.Exception
        throw $_.Exception
        exit
    }
    $Emails = $SecurityContact.Email.Split(",")
    For($index = 0; $index -lt $Emails.Count; $index++) {            
        $Name    = "default" + ($index + 1)
        $Email   = $Emails[$index]
        If($index -eq 0) {
            $Phone = $SecurityContact.Phone
            $Command = "Set-AzureRmSecurityContact -Name $Name -Email $Email -Phone $Phone -AlertAdmin -NotifyOnAlert"
        }
        Else {
            $Phone = $null
            $Command = "Set-AzureRmSecurityContact -Name $Name -Email $Email -Phone $Phone -AlertAdmin -NotifyOnAlert"
        }
        Invoke-Expression -Command $Command
    }
}