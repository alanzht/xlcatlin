#Requires -Module AzureRM.Profile
#Requires -Module AzureRM.Automation

Param
(
    [Parameter(Mandatory=$True)]
    [String] $AutomationResourceGroup,

    [Parameter(Mandatory=$True)]
    [String] $AutomationAccount,

    [Parameter(Mandatory=$True)]
    [String] $NewModule
)

$connectionName = "AzureRunAsConnection"
$ErrorActionPreference = "Stop"

try {
    # Get the connection "AzureRunAsConnection "
    $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName         

    "Logging in to Azure..."
    Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
}
catch {
    if (!$servicePrincipalConnection) {
        $ErrorMessage = "Connection $connectionName not found."
        throw $ErrorMessage
    } 
    else {
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}
$ModuleContentUrl = "https://www.powershellgallery.com/api/v2/package/"

try { 
    $GetModule = Get-AzureRMAutomationModule `
        -ResourceGroupName $AutomationResourceGroup `
        -AutomationAccountName $AutomationAccount `
        | Where-Object {$_.Name -eq $NewModule}
}
catch {
    throw $_.Exception
}

$ModuleExceptionsList = @{}
$ModuleExceptionsList.Add("AzureRM.Security","0.2.0")

If($GetModule) {
    Write-Output "The Module: $NewModule already exists in the automation account: $AutomationAccount"
}
Else {
    Write-Output "The Module: $NewModule does not exist in the automation account: $AutomationAccount. Searching for Latest Version"

    # Add any modules that are not found within the standard search into the ModuleExceptionsList as a Key/Value pair.
    # As of Oct 2018, the AzureRM.Security Module is on Pre-Release version 0.2.0 and therefore is not found within the search
    $moduleVersion = $ModuleExceptionsList[$NewModule]
    If(!$moduleVersion) {
        # Module is not in the Exceptions List
        $Url = "https://www.powershellgallery.com/api/v2/Search()?`$filter=IsLatestVersion&searchTerm=%27$NewModule%27&targetFramework=%27%27&includePrerelease=true&`$skip=0&`$top=40" 
        $SearchResult = Invoke-RestMethod -Method Get -Uri $Url -UseBasicParsing

        if($SearchResult.Length -and $SearchResult.Length -gt 1) {
            $SearchResult = $SearchResult | Where-Object -FilterScript {
                return $_.properties.title -eq $NewModule
            }
        }
        if(!$SearchResult) {
            throw "Could not find module '$NewModule' on PowerShell Gallery."
        }
        $moduleVersion = $SearchResult.properties.version            
    }
    
    $URI = $ModuleContentUrl + $NewModule + "/" + $moduleVersion
    $AutomationModule = New-AzureRmAutomationModule `
        -ResourceGroupName $AutomationResourceGroup `
        -AutomationAccountName $AutomationAccount `
        -Name $NewModule `
        -ContentLinkUri $URI

    while(
        (!([string]::IsNullOrEmpty($AutomationModule))) -and
        $AutomationModule.ProvisioningState -ne "Created" -and
        $AutomationModule.ProvisioningState -ne "Succeeded" -and
        $AutomationModule.ProvisioningState -ne "Failed"
    ) {
        Write-Verbose -Message "Polling for module import completion..."
        Start-Sleep -Seconds 10
        $AutomationModule = $AutomationModule | Get-AzureRmAutomationModule
    }
    if($AutomationModule.ProvisioningState -eq "Failed") {
        Write-Error "Importing $ModuleName module to Automation failed."
    }
    else {
        Write-Output "Importing $ModuleName module to Automation succeeded."
    }
}
