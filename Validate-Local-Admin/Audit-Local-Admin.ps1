$subscriptions = Get-Content -Raw .\subtest.json | ConvertFrom-json
$validateCredsScript = ".\Validate-Credentials.ps1"
$Filename = "Audit-Credentials-Report-" + (Get-Date).tostring("yyyy-MM-dd-hh-mm-ss")
$createFile = New-Item -itemType File -Path ".\" -Name ($Filename + ".csv")
$outputFile = ".\" + $createFile.Name
$PoshSSHModule = "Posh-SSH"
$credentials = @()
$credentials += @{username='XLSupport';Password='aA1234567890'}
$credentials += @{username='XLSupport';Password='Conr0170'}

$Module = Get-Module -ListAvailable | Where {$_.Name -eq $PoshSSHModule}
If(!$Module) {
    $result = Find-Module $PoshSSHModule | Install-Module
    If(!$result) {        
        Write-Error "Could not install Module: $PoshSSHModule"
    }
}
ForEach($sub in $subscriptions) {
    $subscriptionID = $sub.Id
    Select-AzureRMSubscription -subscriptionID $subscriptionID
    $VMList = Get-AzureRMVM
    ForEach($VM in $VMList) {
        ForEach($cred in $credentials) {
            $username = $cred.username
            $password = $cred.password
            $securePass = ConvertTo-SecureString $password -AsPlainText -Force
            $credObj = New-Object System.Management.Automation.PSCredential($username,$securePass)
            $isWindows = $VM.OSProfile.WindowsConfiguration
            If($isWindows) {
                # Execute Invoke-AzureRMVMRunCommand with Validate Credentials Script on Remote VM
                $result = Invoke-AzureRmVMRunCommand -ResourceGroupName $VM.ResourceGroupName -VMName $VM.Name `
                         -ScriptPath $validateCredsScript -Parameter @{"Username" = $username; "Password" = $password} -CommandId 'RunPowerShellScript' -Verbose
                "Subscription: $($sub.Name) | VM: $($VM.Name) | Username: $username | Password: $password | isValid: $($result.Value.Message)" | Add-Content -Path $outputFile
            }
            Else {
                #Obtain the First Network Interface, as we assume this is the main one containing the connection address
                $NIC = Get-AzureRmResource -ResourceId $VM.NetworkProfile.NetworkInterfaces[0].Id | Get-AzureRmNetworkInterface
                $PrivateIP = $NIC.IpConfigurations.PrivateIPAddress
                If($PrivateIP) {
                    $result = New-SSHSession -ComputerName $PrivateIP -Credential $credObj -Port 22 -Force
                    If($result) {
                        "Subscription: $($sub.Name) | VM: $($VM.Name) | IPAddress: $($result.Host) | Username: $username | Password: $password | isValid: $($result.Value.Message)" | Add-Content -Path $outputFile
                        Remove-SSHSession -SSHSession $result
                    }
                    Else {
                        "Subscription: $($sub.Name) | VM: $($VM.Name) | IPAddress: $($result.Host) | Username: $username | Password: $password | isValid: $False" | Add-Content -Path $outputFile
                    }
                }
                Else {
                    Write-Output "Subscription: $($sub.Name) | VM: $($VM.Name) -- Error finding Private IP Address... Skipping"
                }
            }
        }        
    }
}