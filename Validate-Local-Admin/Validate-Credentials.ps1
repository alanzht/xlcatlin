param(
    [Parameter(Mandatory = $true)]
    [string]$username,
    
    [Parameter(Mandatory = $true)]
    [string]$password
)
Add-Type -AssemblyName System.DirectoryServices.AccountManagement
$obj = New-Object System.DirectoryServices.AccountManagement.PrincipalContext('machine',$(hostname))

$isValid = $obj.ValidateCredentials($username, $password)
Return $isValid
